import { TemplateFileStatsInterface } from './src/types/files.types';
import { readdir, readFile } from 'fs/promises';
import { getClassicTemplateData, templatesFolderRoot } from './src/utils/files.utils';
import { ResumeInputInterface } from './src/types/resume-input.types';
import { PreviewLinksType } from './src/types/template.types';
import { ResumeGenerator } from './src/lib/resume-generator.lib';

const templateName = process.argv.slice(2)[0];

const getTemplateFile = async (): Promise<TemplateFileStatsInterface> => {
	const dirContent = await readdir(templatesFolderRoot);
	const templateFileName = dirContent.filter((tempName: string) => tempName.includes(templateName))[0];

	if (!templateFileName) {
		throw new Error(`Couldn't find template with the name: ${templateName}`);
	}

	return await getClassicTemplateData(templateFileName);
};

const getInput = async (): Promise<ResumeInputInterface> => {
	const input = await readFile(`${templatesFolderRoot}/template-filler.json`, {
		encoding: 'utf-8'
	});
	return JSON.parse(input) as ResumeInputInterface;
};

const generatePreview = async (templateFile: TemplateFileStatsInterface): Promise<PreviewLinksType> => {
	const generator: ResumeGenerator = new ResumeGenerator(templateFile.codeName);
	generator.lang = 'fr';
	generator.dataForm = await getInput();
	generator.templatePath = templateFile.path;

	return await generator.generateTemplatePreviews('fr');
};

void (async () => {
	const templateFile: TemplateFileStatsInterface = await getTemplateFile();
	await generatePreview(templateFile);
})();
