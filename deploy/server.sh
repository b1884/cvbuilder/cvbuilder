cd ~/cv-template.co/cvbuilder
git checkout master
git pull origin master

NODE_ENV="whatever_not_prod" yarn install
yarn build
pm2 restart cvbuilder || pm2 start ~/cv-template.co/cvbuilder/dist/src/index.js --name "cvbuilder"
