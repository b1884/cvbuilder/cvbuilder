#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
DEPLOY_USER=$DEPLOY_USER
SSH_PORT=$SSH_PORT

echo "Deploying the server to ${DEPLOY_SERVER}"
ssh ${DEPLOY_USER}@${DEPLOY_SERVER} -p ${SSH_PORT} 'bash -i ' < ./deploy/server.sh
