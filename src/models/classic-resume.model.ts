import { Document, Model, Types } from 'mongoose';
import { LangType } from '../types/lang.types';
import { ResumeInputInterface } from '../types/resume-input.types';

export interface ClassicResumeInterface {
	_id?: string | Types.ObjectId;
	userId: string | Types.ObjectId;
	templateCodeName: string;
	title: string;
	link: string;
	lang: LangType;
	sharingLink: string;
	isShareable?: boolean;
	rawData: ResumeInputInterface;
	profileImage?: string | null;
	previewImage?: string;
	creationDate?: Date;
	lastUpdateDate: Date;
}

export type ClassicResumePreviewType = Omit<ClassicResumeInterface, 'rawData'>;

export type ClassicResumeType = ClassicResumeInterface & Document;

export interface ClassicResumeModelInterface extends Model<ClassicResumeType> {
	newClassicResume(body: Partial<ClassicResumeInterface>): Promise<ClassicResumeInterface>;

	getMany(query: Partial<ClassicResumeInterface>): Promise<ClassicResumeInterface[]>;

	getPreviews(query: Partial<ClassicResumeInterface>): Promise<ClassicResumePreviewType[]>;

	getOne(query: Partial<ClassicResumeInterface>): Promise<ClassicResumeInterface | null>;

	updateOneClassicResume(body: Partial<ClassicResumeInterface>): Promise<ClassicResumeInterface | null>;

	removeOne(body: Partial<ClassicResumeInterface>): Promise<ClassicResumeInterface | null>;

	removeAllByUserId(body: Pick<ClassicResumeInterface, 'userId'>): Promise<ClassicResumeInterface | null>;
}
