import { Document, Model } from 'mongoose';
import { LangType } from '../types/lang.types';

export interface UserInterface {
	_id?: string;
	name: string;
	lastName: string;
	email: string;
	unConfirmedEmail?: string | null;
	subscriptionType?: string;
	subscriptionStartTime?: Date;
	creationDate?: Date;
}

export interface NewUserResponseInterface extends UserInterface {
	resumeLink: string;
}

export type UserType = UserInterface & Document;

export interface UserModelInterface extends Model<UserType> {
	newUser(body: UserInterface, templateCodeName?: string, lang?: LangType): Promise<NewUserResponseInterface>;

	getMany(query: Partial<UserInterface>): Promise<UserInterface[]>;

	getOne(query: Partial<UserInterface>): Promise<UserInterface | null>;

	updateOneUser(body: Partial<UserInterface>): Promise<UserInterface | null>;

	removeOne(body: Partial<UserInterface>): Promise<UserInterface | null>;
}
