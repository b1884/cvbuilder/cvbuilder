import { Document, Model } from 'mongoose';

export interface TemplateInterface {
	_id?: string;
	name: string;
	codeName: string;
	previewSm: string;
	previewXl: string;
	lang: string;
	creationDate?: Date;
	lastUpdateDate?: Date;
}

export type TemplateType = TemplateInterface & Document;

export interface TemplateModelInterface extends Model<TemplateType> {
	newTemplate(body: TemplateInterface): Promise<TemplateInterface>;

	getMany(query?: Partial<TemplateInterface>): Promise<TemplateInterface[]>;

	getOne(query: Partial<TemplateInterface>): Promise<TemplateInterface | null>;

	updateOneTemplate(body: Partial<TemplateInterface>): Promise<TemplateInterface | null>;

	removeOne(body: Partial<TemplateInterface>): Promise<TemplateInterface | null>;
}
