import { CvBuilderGlobalType } from './types/cv-builder-global.types';
import { createClient } from 'redis';

(<CvBuilderGlobalType>global).__appRoot = __dirname.replace('/src', '');

import express, { Express } from 'express';
import api from './api';
import { connect } from 'mongoose';
import { urlencoded, json } from 'body-parser';
import { config } from 'dotenv';
import cors, { CorsOptions } from 'cors';
import { TemplatesChecker } from './lib/templates-checker.lib';
import compression from 'compression';
import cookieParser from 'cookie-parser';

// loading environment file <.env>
config();

const options: CorsOptions = {
	allowedHeaders: [
		'Origin',
		'X-Requested-With',
		'Content-Type',
		'Accept',
		'X-Access-Token',
		'Authorization',
		'x-client-language'
	],
	credentials: true,
	methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
	origin:
		process.env.NODE_ENV === 'production'
			? [
					'https://cv-template.co',
					'https://app.cv-template.co',
					'https://www.cv-template.co',
					'https://www.app.cv-template.co'
			  ]
			: [
					'https://www.cv-template.xyz',
					'https://www.app.cv-template.xyz',
					'https://cv-template.xyz',
					'https://app.cv-template.xyz'
			  ]
};

// eslint-disable-next-line @typescript-eslint/no-unsafe-call
const app: Express = express() as Express;
const { PORT, NODE_ENV, MONGO_URL, MONGO_URL_DEV } = process.env;
const DB_URL: string = NODE_ENV === 'development' ? MONGO_URL_DEV : MONGO_URL;
const port: number = +PORT;
/*const shouldCompress = (req: Request, res: Response): boolean => {
	if (req.headers['x-no-compression']) {
		// don't compress responses with this request header
		return false;
	}

	// fallback to standard filter function
	return compression.filter(req, res);
};*/

const initServer = async () => {
	await connect(DB_URL);

	(<CvBuilderGlobalType>global).redisClient = createClient();
	await (<CvBuilderGlobalType>global).redisClient.connect();
	// initLocations();
	// eslint-disable-next-line @typescript-eslint/no-unsafe-call
	app.use(cors(options));
	app.use(cookieParser());
	app.use(compression({ level: 9 }));
	app.use(urlencoded({ extended: false }));
	app.use(json());
	app.use(NODE_ENV === 'production' ? '/api' : '/', api);

	app.listen(port, '0.0.0.0', () => {
		console.log(`server started at 0.0.0.0:${port}`);
	});
};

initServer()
	.then(async () => {
		console.log('started the server!');
		const templatesChecker = new TemplatesChecker();
		await templatesChecker.getTemplatesFromDB();
		await templatesChecker.getTemplatesFiles();
		await templatesChecker.removeNonExistingTemplates();
		await templatesChecker.updateExistingTemplate();
		await templatesChecker.addNewTemplates();
		console.info(
			`Templates init done: \n • ${templatesChecker.templatesFiles.length} template files found and synced with DB.`
		);
	})
	.catch((e) => {
		console.error(e);
	});
