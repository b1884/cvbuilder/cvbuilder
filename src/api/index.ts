import express from 'express';
import { authRouter, usersRouter, classicBuilderRouter } from './routes';
import { mediaRoot, personalPhotosRoot, templatesAssetsRoot, templatesPreviewRoot } from '../utils/files.utils';

const api = express();

// we may add specific middlewares here

api.use('/users', usersRouter);
api.use('/auth', authRouter);
api.use('/classic-builder', classicBuilderRouter);

api.use('/classic_media', express.static(`${mediaRoot}`));
api.use('/classic_previews', express.static(`${templatesPreviewRoot}`));
api.use('/personal_photos', express.static(`${personalPhotosRoot}`));
api.use('/templates_assets', express.static(`${templatesAssetsRoot}`));

export default api;
