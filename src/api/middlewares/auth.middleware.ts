import { CvBuilderRequestInterface } from '../../types/request.types';
import { NextFunction, Response } from 'express';
import { JwtPayload, verify } from 'jsonwebtoken';
import { responseHandler } from '../../utils/response.utils';
import { Result } from '../../utils/result.utils';
import { UserInterface } from '../../models';
// import { isTokenWhitelisted } from '../../utils/auth.utils';

export const AuthMiddleware = (req: CvBuilderRequestInterface, res: Response, next: NextFunction | undefined): void => {
	if (!next) {
		throw new Error('next function is required for middlewares!');
	}

	const { SECRET } = process.env;

	const accessToken = req.cookies.handler_crow || '';
	if (!accessToken) {
		responseHandler(res, Result.fail(`User isn't authenticated.`, 403));
		return;
	}

	// if (!isTokenWhitelisted(accessToken)) {
	// 	responseHandler(
	// 		res.clearCookie('handler_crow').clearCookie('ext'),
	// 		Result.fail(`User isn't authenticated.`, 403)
	// 	);
	// 	return;
	// }

	const authPayload = verify(accessToken, SECRET) as JwtPayload;
	if (!authPayload) {
		responseHandler(res, Result.fail(`User isn't authenticated.`, 403));
		return;
	}

	req.user = authPayload.user as Pick<UserInterface, '_id'>;
	next();
	return;
};
