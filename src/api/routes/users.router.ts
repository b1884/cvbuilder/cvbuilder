import { Router } from 'express';
import {
	getUser,
	getUsers,
	deleteUser,
	updateUser,
	getCurrentUser,
	verifyNewEmail
} from '../controllers/users.controller';
import { toControllerFunction } from '../../utils/controller.utils';
import { AuthMiddleware } from '../middlewares/auth.middleware';

export const usersRouter = Router();

usersRouter.get('/single', AuthMiddleware, toControllerFunction(getUser));
usersRouter.get('/current', AuthMiddleware, toControllerFunction(getCurrentUser));
usersRouter.get('/multiple', AuthMiddleware, toControllerFunction(getUsers));
usersRouter.delete('/purge-user', AuthMiddleware, toControllerFunction(deleteUser));
usersRouter.patch('/update-user', AuthMiddleware, toControllerFunction(updateUser));
usersRouter.post('/verify-new-email', toControllerFunction(verifyNewEmail));

// usersRouter.get('/token-data', AuthMiddleware.verifyToken, getMe);

// usersRouter.get('/current', AuthMiddleware.verifyToken, getCurrentUser);
// usersRouter.post('/avatar', AuthMiddleware.verifyToken, uploadImage.single('file'), uploadAvatar);

// usersRouter.put('/', AuthMiddleware.verifyToken, uploadImage.single('profilePicture'), updateUser);
