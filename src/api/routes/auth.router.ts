import { Router } from 'express';
import {
	checkIfEmailExist,
	getInfoFromLinkedIn,
	setLanguage,
	signIn,
	signOut,
	signUp,
	verifyEmail
} from '../controllers/auth.controller';
import { toControllerFunction } from '../../utils/controller.utils';
import {
	CheckEmailExistPayloadInterface,
	GetLinkedinTokenPayloadInterface,
	SignInPayloadInterface,
	SignUpPayloadInterface,
	VerifyEmailPayloadInterface
} from '../../types/auth.types';

export const authRouter = Router();

authRouter.post('/sign-up', toControllerFunction<SignUpPayloadInterface>(signUp));
authRouter.post('/verify-email', toControllerFunction<VerifyEmailPayloadInterface>(verifyEmail));
authRouter.post('/check-email-exist', toControllerFunction<CheckEmailExistPayloadInterface>(checkIfEmailExist));
authRouter.post('/sign-in', toControllerFunction<SignInPayloadInterface>(signIn));
authRouter.post('/sign-out', toControllerFunction(signOut));
authRouter.post('/set-language', setLanguage);
authRouter.post('/get-linkedin-token', toControllerFunction<GetLinkedinTokenPayloadInterface>(getInfoFromLinkedIn));
