import { Router } from 'express';
import { toControllerFunction } from '../../utils/controller.utils';
import {
	buildClassicResumePDFBuffer,
	createNewResume,
	deleteResume,
	getClassicResumeByLink,
	getClassicResumePDF,
	getClassicResumePreviewsBySharingLink,
	getClassicResumesPreviews,
	getTemplates,
	removePersonalPhoto,
	updateClassicResume,
	uploadPersonalPhoto
} from '../controllers/classic-builder.controller';
import { ClassicResumeInterface, ClassicResumePreviewType } from '../../models';
import { AuthMiddleware } from '../middlewares/auth.middleware';
import { uploadImage } from '../../utils/uploader.utils';

export const classicBuilderRouter = Router();

classicBuilderRouter.post(
	'/build-pdf-buffer',
	AuthMiddleware,
	toControllerFunction<ClassicResumeInterface, Buffer>(buildClassicResumePDFBuffer)
);

classicBuilderRouter.post('/create-new', AuthMiddleware, toControllerFunction(createNewResume));

classicBuilderRouter.get(
	'/current',
	AuthMiddleware,
	toControllerFunction<unknown, Pick<ClassicResumeInterface, 'link'>>(getClassicResumeByLink)
);

classicBuilderRouter.get(
	'/share-resume',
	toControllerFunction<unknown, Pick<ClassicResumeInterface, 'sharingLink'>>(getClassicResumePreviewsBySharingLink)
);

classicBuilderRouter.get(
	'/download',
	AuthMiddleware,
	toControllerFunction<unknown, Pick<ClassicResumeInterface, 'link'>>(getClassicResumePDF)
);

classicBuilderRouter.get('/templates', toControllerFunction(getTemplates));

classicBuilderRouter.get('/previews-list', AuthMiddleware, toControllerFunction(getClassicResumesPreviews));

classicBuilderRouter.patch(
	'/update-resume',
	AuthMiddleware,
	toControllerFunction<Partial<ClassicResumePreviewType>>(updateClassicResume)
);

classicBuilderRouter.patch(
	'/upload-personal-photo',
	AuthMiddleware,
	uploadImage,
	toControllerFunction<Partial<ClassicResumePreviewType>>(uploadPersonalPhoto)
);

classicBuilderRouter.delete(
	'/remove-personal-photo',
	AuthMiddleware,
	toControllerFunction<Partial<ClassicResumePreviewType>>(removePersonalPhoto)
);

classicBuilderRouter.delete(
	'/delete-resume',
	AuthMiddleware,
	toControllerFunction<Partial<ClassicResumePreviewType>>(deleteResume)
);
