import { CookieOptions, Response } from 'express';
import { ClassicResumes, Users } from '../../schema';
import { ClassicResumePreviewType, UserInterface } from '../../models';
import { CvBuilderRequestInterface } from '../../types/request.types';
import { responseHandler } from '../../utils/response.utils';
import { Result } from '../../utils/result.utils';
import { handleCatch } from '../../utils/controller.utils';
import { JwtPayload, sign, verify } from 'jsonwebtoken';
import { deleteString, hashString, revokeToken, unHashString, whiteListToken } from '../../utils/auth.utils';
import { Mailer } from '../../utils/mail.utils';
import {
	SignInResponseInterface,
	VerifyEmailPayloadInterface,
	VerifyNewEmailPayloadInterface
} from '../../types/auth.types';
import { isPathReal, mediaRoot, personalPhotosRoot } from '../../utils/files.utils';
import { rm } from 'fs/promises';
import moment from 'moment';

export const getUsers = async (
	req: CvBuilderRequestInterface<unknown, Partial<UserInterface>>,
	res: Response
): Promise<void> => {
	try {
		const users: UserInterface[] = await Users.getMany(req.query);
		responseHandler<UserInterface[]>(res, Result.ok<UserInterface[]>(users));
	} catch (e) {
		handleCatch(res, e);
	}
};

export const getCurrentUser = async (req: CvBuilderRequestInterface, res: Response): Promise<void> => {
	try {
		if (!req.user || !req.user._id) {
			responseHandler(res, Result.fail('No authenticated user', 403));
			return;
		}

		const user = await Users.getOne({ _id: req.user._id });

		if (!user) {
			responseHandler(res, Result.fail('User not found', 404));
			return;
		}

		responseHandler<UserInterface>(res, Result.ok<UserInterface>(user));
	} catch (e) {
		handleCatch(res, e);
	}
};

export const getUser = async (
	req: CvBuilderRequestInterface<unknown, Partial<UserInterface>>,
	res: Response
): Promise<void> => {
	try {
		const user = await Users.getOne(req.query);

		if (!user) {
			responseHandler(res, Result.fail('User not found', 404));
			return;
		}

		responseHandler<UserInterface>(res, Result.ok<UserInterface>(user));
	} catch (e) {
		handleCatch(res, e);
	}
};

export const updateUser = async (
	req: CvBuilderRequestInterface<Partial<UserInterface>>,
	res: Response
): Promise<void> => {
	const { SECRET } = process.env;

	try {
		if (!req.body) {
			responseHandler(res, Result.fail(`Wrong body or no body at all`, 400));
		}

		if (!req.user || !req.user._id) {
			responseHandler(res, Result.fail('User should be authenticated', 401));
			return;
		}

		const currentUser: UserInterface | null = await Users.getOne({ _id: req.user._id });

		if (!currentUser) {
			responseHandler(res, Result.fail(`User should be authenticated`, 401));
			return;
		}

		if (req.body.email) {
			const userByEmail: UserInterface | null = await Users.getOne({ email: req.body.email });

			if (userByEmail) {
				responseHandler<UserInterface>(
					res,
					Result.fail(
						`(user-already-exists) User corresponding to the email ${req.body.email} already exists!`,
						409
					)
				);
				return;
			}

			const temporaryToken: string = sign({ email: currentUser.email }, SECRET, { expiresIn: '30m' });
			const tokenForEmail = await hashString(temporaryToken);

			const mailer = new Mailer('verifyEmail');
			await mailer.sendMail(
				req.body.email,
				mailer.recepientFullName(currentUser),
				'Vérifiez votre nouvelle adresse e-mail!',
				mailer.getTemplate(
					{
						verifyEmail: `${
							process.env.NODE_ENV === 'production'
								? 'https://cv-template.co'
								: 'https://www.cv-template.xyz'
						}/?verify_email=${tokenForEmail}`
					},
					currentUser
				)
			);

			req.body.unConfirmedEmail = req.body.email;
		}

		const { email, ...others } = req.body;

		const payload: Partial<UserInterface> = others;

		payload._id = req.user?._id;

		const user: UserInterface | null = await Users.updateOneUser(payload);
		const msg = user ? '' : 'No changes on user.';
		responseHandler(res, user ? Result.ok<UserInterface>(user) : Result.ok(), msg);
	} catch (e) {
		handleCatch(res, e);
	}
};

export const deleteUser = async (req: CvBuilderRequestInterface, res: Response): Promise<void> => {
	if (!req.user || !req.user._id) {
		responseHandler(res, Result.fail('User should be authenticated', 401));
		return;
	}

	const currentUser = await Users.getOne({ _id: req.user._id });

	if (!currentUser) {
		responseHandler(res, Result.fail(`User should be authenticated`, 401));
		return;
	}

	try {
		const cookieOptions: CookieOptions = {
			domain: process.env.NODE_ENV === 'production' ? '.cv-template.co' : '.cv-template.xyz',
			expires: moment().add(7, 'days').toDate(),
			secure: process.env.NODE_ENV === 'production'
		};

		await revokeToken(req.cookies.handler_crow || '');
		responseHandler(
			res
				.cookie('handler_crow', '', {
					...{ ...cookieOptions, expires: new Date(1979, 1, 1) },
					httpOnly: true
				})
				.cookie('exp', '', { ...cookieOptions, expires: new Date(1979, 1, 1) }),
			Result.ok()
		);
	} catch (e) {
		handleCatch(res, e);
	}

	const resumesRoot = `${mediaRoot}/${req.user._id}`;
	const photosRoot = `${personalPhotosRoot}/${req.user._id}`;

	const hasResumes = await isPathReal(resumesRoot);
	const hasPhotos = await isPathReal(photosRoot);

	if (hasResumes) {
		await rm(resumesRoot, { recursive: true });
	}

	if (hasPhotos) {
		await rm(photosRoot, { recursive: true });
	}

	await ClassicResumes.removeAllByUserId({ userId: req.user._id });

	await Users.removeOne(req.user);

	const mailer = new Mailer('deleted');
	await mailer.sendMail(
		currentUser.email,
		mailer.recepientFullName(currentUser),
		'Triste de vous voir partir !',
		mailer.getTemplate({}, currentUser)
	);
};

export const verifyNewEmail = async (
	req: CvBuilderRequestInterface<VerifyNewEmailPayloadInterface>,
	res: Response
): Promise<void> => {
	const { SECRET } = process.env;

	if (!req.body.verifyEmail) {
		responseHandler(res, Result.fail('Link expired', 410));
		return;
	}
	const { verifyEmail } = req.body;
	const tempoToken = await unHashString(verifyEmail);

	if (!tempoToken) {
		responseHandler(res, Result.fail('Link expired', 410));
		return;
	}

	const payload = verify(tempoToken, SECRET) as JwtPayload;

	if (!payload || !payload.email) {
		responseHandler(res, Result.fail('Link expired', 410));
		return;
	}

	try {
		const user = await Users.getOne(payload as { email: string });

		if (!user) {
			responseHandler(res, Result.fail('User not found', 404));
			return;
		}

		if (!user.unConfirmedEmail) {
			responseHandler(res, Result.ok());
			return;
		}

		const userByUnconfirmedEmail: UserInterface | null = await Users.getOne({ email: user.unConfirmedEmail });
		if (userByUnconfirmedEmail) {
			const updatePayload: Partial<UserInterface> = {
				_id: user._id,
				email: user.email,
				unConfirmedEmail: null
			};

			await Users.updateOneUser(updatePayload);

			responseHandler<UserInterface>(
				res,
				Result.fail(
					`(user-already-exists) User corresponding to the email ${user.unConfirmedEmail} already exists!`,
					409
				)
			);
			return;
		}

		const updatePayload: Partial<UserInterface> = {
			_id: user._id,
			email: user.unConfirmedEmail,
			unConfirmedEmail: null
		};

		await Users.updateOneUser(updatePayload);
		responseHandler(res, Result.ok());
		await deleteString(verifyEmail);
		return;
	} catch (e) {
		handleCatch(res, e);
	}
};
