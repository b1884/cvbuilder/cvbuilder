import { Response } from 'express';
import { ClassicResumes, Templates, Users } from '../../schema';
import {
	ClassicResumeInterface,
	ClassicResumePreviewType,
	NewUserResponseInterface,
	TemplateInterface,
	UserInterface
} from '../../models';
import { CvBuilderRequestInterface } from '../../types/request.types';
import { responseHandler } from '../../utils/response.utils';
import { Result } from '../../utils/result.utils';
import { handleCatch } from '../../utils/controller.utils';
import {
	emptyMakeDir,
	isPathReal,
	personalPhotosFolderPath,
	resumeFoldersPath,
	resumeImagesFolderPath,
	resumePdfFolderPath,
	templatePath
} from '../../utils/files.utils';
import { ClassicGeneratorOutputInterface, ResumeGenerator } from '../../lib/resume-generator.lib';
import { resizeImage } from '../../utils/uploader.utils';
import { unlink, rm, readdir } from 'fs/promises';
import { ResumeInputInterface } from '../../types/resume-input.types';
import { generateNewUserRawData } from '../../utils/resume.utils';
import { LangType } from '../../types/lang.types';

export const getTemplates = async (req: CvBuilderRequestInterface, res: Response): Promise<void> => {
	try {
		const desiredLanguage = req.headers['x-client-language'] ? req.headers['x-client-language'] : 'fr';
		const templates: TemplateInterface[] = await Templates.getMany({ lang: desiredLanguage as LangType });

		responseHandler<TemplateInterface[]>(
			res,
			Result.ok<TemplateInterface[]>(
				templates.sort((a, b) => {
					if (a.name < b.name) {
						return -1;
					}
					if (a.name > b.name) {
						return 1;
					}
					return 0;
				})
			)
		);
	} catch (e) {
		handleCatch(res, e);
	}
};

export const createNewResume = async (req: CvBuilderRequestInterface, res: Response): Promise<void> => {
	if (!req.user) {
		responseHandler(res, Result.fail('User not authenticated', 403));
		return;
	}

	const currentUser: UserInterface | null = await Users.getOne({ _id: req.user._id });

	if (!currentUser) {
		responseHandler(res, Result.fail("Couldn't find user!", 404));
		return;
	}

	const newRawData = generateNewUserRawData(currentUser);
	const newResumeBody: Partial<ClassicResumeInterface> = {
		userId: currentUser._id,
		lang: newRawData.language,
		templateCodeName: 'zinnia',
		title: newRawData.title,
		rawData: newRawData
	};
	const newResume = await ClassicResumes.newClassicResume(newResumeBody);

	responseHandler(
		res,
		Result.ok<Pick<NewUserResponseInterface, 'resumeLink'>>({
			resumeLink: newResume.link
		})
	);
	return;
};

export const getClassicResumesPreviews = async (req: CvBuilderRequestInterface, res: Response): Promise<void> => {
	try {
		if (!req.user || !req.user._id) {
			responseHandler(res, Result.fail('User should be authenticated', 401));
			return;
		}

		const classicResumes: ClassicResumePreviewType[] = await ClassicResumes.getPreviews({ userId: req.user._id });

		if (!classicResumes || !classicResumes.length) {
			responseHandler(res, Result.ok([]));
			return;
		}

		responseHandler<ClassicResumePreviewType[]>(res, Result.ok<ClassicResumePreviewType[]>(classicResumes));
	} catch (e) {
		handleCatch(res, e);
	}
};

export const getClassicResumeByLink = async (
	req: CvBuilderRequestInterface<unknown, Pick<ClassicResumeInterface, 'link'>>,
	res: Response
): Promise<void> => {
	if (!req.query || !req.query.link) {
		responseHandler(res, Result.fail('Need to specify doc link', 400));
		return;
	}

	if (!req.user || !req.user._id) {
		responseHandler(res, Result.fail('User should be authenticated', 401));
		return;
	}

	const classicResume: ClassicResumeInterface | null = await ClassicResumes.getOne({
		userId: req.user._id,
		link: req.query.link
	});

	if (!classicResume) {
		responseHandler(res, Result.fail(`resume doesn't exist or deleted`, 404));
		return;
	}

	responseHandler<ClassicResumeInterface>(res, Result.ok<ClassicResumeInterface>(classicResume));
};

export const getClassicResumePreviewsBySharingLink = async (
	req: CvBuilderRequestInterface<unknown, Pick<ClassicResumeInterface, 'sharingLink'>>,
	res: Response
): Promise<void> => {
	if (!req.query || !req.query.sharingLink) {
		responseHandler(res, Result.fail('Need to specify doc link', 400));
		return;
	}

	const classicResume: ClassicResumeInterface | null = await ClassicResumes.getOne({
		sharingLink: req.query.sharingLink
	});

	if (!classicResume) {
		responseHandler(res, Result.fail(`resume doesn't exist or deleted`, 404));
		return;
	}

	const resumeImagesFolder = resumeImagesFolderPath(classicResume._id as string, classicResume.userId as string);
	const hasResumeImagesFolder = await isPathReal(resumeImagesFolder);

	if (!hasResumeImagesFolder) {
		responseHandler(res, Result.fail(`Resume is not generated yet.`, 404));
		return;
	}
	const dirContent = await readdir(resumeImagesFolder);

	if (!dirContent || !dirContent.length) {
		responseHandler(res, Result.fail(`Resume is not generated yet.`, 404));
		return;
	}

	const resumePagesFilesNames = dirContent.filter((imageName: string) => !imageName.includes('preview_'));

	responseHandler<string[]>(
		res,
		Result.ok<string[]>(
			resumePagesFilesNames.map(
				(image: string) => `${classicResume.userId as string}/${classicResume._id as string}/images/${image}`
			)
		)
	);
	return;
};

export const getClassicResumePDF = async (
	req: CvBuilderRequestInterface<unknown, Pick<ClassicResumeInterface, 'link'>>,
	res: Response
): Promise<void> => {
	if (!req.query || !req.query.link) {
		responseHandler(res, Result.fail('Need to specify doc link', 400));
		return;
	}

	if (!req.user || !req.user._id) {
		responseHandler(res, Result.fail('User should be authenticated', 401));
		return;
	}

	const classicResume: ClassicResumeInterface | null = await ClassicResumes.getOne({
		link: req.query.link,
		userId: req.user._id
	});

	if (!classicResume) {
		responseHandler(res, Result.fail(`resume doesn't exist or deleted`, 404));
		return;
	}

	const resumePdfFolder = resumePdfFolderPath(classicResume._id as string, classicResume.userId as string);
	await emptyMakeDir(resumePdfFolder);

	const generator = new ResumeGenerator(classicResume.title);
	const templateName = classicResume.templateCodeName.split('_')[0];
	generator.lang = classicResume.lang || (req.headers['x-client-language'] as LangType);
	generator.templatePath = templatePath(templateName);

	if (classicResume.profileImage) {
		classicResume.rawData.profileImage = `personal_photos/${req.user?._id}/${classicResume.profileImage}`;
	}

	generator.dataForm = classicResume.rawData;

	const pdfFileName = await generator.generatePDF(resumePdfFolder);

	responseHandler<string>(res, Result.ok<string>(pdfFileName));
	return;
};

export const getClassicResume = async (
	req: CvBuilderRequestInterface<unknown, Partial<ClassicResumeInterface>>,
	res: Response
): Promise<void> => {
	try {
		if (!req.query || !req.query._id || !req.query.userId) {
			responseHandler(res, Result.fail('Need to specify doc id and user id', 400));
			return;
		}

		if (!req.user || !req.user._id || req.user._id !== req.query.userId) {
			responseHandler(res, Result.fail('User should be authenticated', 401));
			return;
		}

		const classicResume: ClassicResumeInterface | null = await ClassicResumes.getOne({
			userId: req.user._id,
			_id: req.query._id
		});

		if (!classicResume) {
			responseHandler(res, Result.fail(`resume doesn't exist or deleted`, 404));
			return;
		}

		responseHandler<ClassicResumeInterface>(res, Result.ok<ClassicResumeInterface>(classicResume));
	} catch (e) {
		handleCatch(res, e);
	}
};

export const buildClassicResumePDFBuffer = async (
	req: CvBuilderRequestInterface<ClassicResumeInterface>,
	res: Response<Buffer>
): Promise<void> => {
	try {
		if (!req.body) {
			responseHandler(res, Result.fail(`Can't generate on empty stomach!`, 400));
		}

		const resumeBody: ClassicResumeInterface = req.body;
		await ClassicResumes.updateOneClassicResume({
			...resumeBody
		});

		const imagesFolder = resumeImagesFolderPath(resumeBody._id as string, resumeBody.userId as string);

		const generator = new ResumeGenerator(resumeBody.link, imagesFolder);
		const templateName = resumeBody.templateCodeName.split('_')[0];
		generator.templatePath = templatePath(templateName);

		const resumeData: ResumeInputInterface = Object.assign({}, resumeBody.rawData);

		if (resumeBody.profileImage) {
			resumeData.profileImage = `personal_photos/${req.user?._id}/${resumeBody.profileImage}`;
		}
		generator.lang = resumeData.language;
		generator.dataForm = resumeData;
		const genRes: Buffer = await generator.generatePDFForBuilder();

		res.status(200).send(genRes);

		const imagesResult: ClassicGeneratorOutputInterface = await generator.generateResumeImages(genRes);
		await ClassicResumes.updateOneClassicResume({
			_id: resumeBody._id,
			userId: resumeBody.userId,
			previewImage: imagesResult.previewImage
		});
		return;
	} catch (e) {
		handleCatch(res, e);
	}
};

export const updateClassicResume = async (
	req: CvBuilderRequestInterface<Partial<ClassicResumePreviewType>>,
	res: Response
): Promise<void> => {
	try {
		if (!req.body) {
			responseHandler(res, Result.fail(`Wrong body or no body at all`, 400));
		}

		const resumeBody: Partial<ClassicResumePreviewType> = req.body;

		resumeBody.userId = req.user?._id;

		await ClassicResumes.updateOneClassicResume(resumeBody);

		responseHandler(res, Result.ok());
		return;
	} catch (e) {
		handleCatch(res, e);
	}
};

export const uploadPersonalPhoto = async (
	req: CvBuilderRequestInterface<Partial<ClassicResumePreviewType>>,
	res: Response
): Promise<void> => {
	try {
		if (!req.body) {
			responseHandler(res, Result.fail(`Wrong body or no body at all`, 400));
			return;
		}

		if (!req.file) {
			responseHandler(res, Result.fail(`No file uploaded`, 400));
			return;
		}

		const resumeBody: Partial<ClassicResumePreviewType> | null = await ClassicResumes.getOne({ _id: req.body._id });

		if (!resumeBody) {
			responseHandler(res, Result.fail(`Wrong resume id`, 400));
			return;
		}

		const oldFilePath =
			resumeBody.profileImage && resumeBody.profileImage.length
				? `${personalPhotosFolderPath(req.user?._id)}/${resumeBody.profileImage}`
				: null;
		const isFileExisting = oldFilePath ? await isPathReal(oldFilePath) : false;

		if (isFileExisting && oldFilePath) {
			await unlink(oldFilePath);
		}

		resumeBody.userId = req.user?._id;
		resumeBody.profileImage = await resizeImage(req.file);

		await ClassicResumes.updateOneClassicResume(resumeBody);

		responseHandler(
			res,
			Result.ok<Required<Pick<ClassicResumePreviewType, 'profileImage'>>>({
				profileImage: resumeBody.profileImage
			})
		);
		return;
	} catch (e) {
		handleCatch(res, e);
	}
};

export const removePersonalPhoto = async (
	req: CvBuilderRequestInterface<Partial<ClassicResumePreviewType>>,
	res: Response
): Promise<void> => {
	try {
		if (!req.body) {
			responseHandler(res, Result.fail(`Wrong body or no body at all`, 400));
			return;
		}

		const resumeBody: Partial<ClassicResumePreviewType> | null = await ClassicResumes.getOne({ _id: req.body._id });

		if (!resumeBody) {
			responseHandler(res, Result.fail(`Wrong resume id`, 400));
			return;
		}

		const fullPath =
			resumeBody.profileImage && resumeBody.profileImage.length
				? `${personalPhotosFolderPath(req.user?._id)}/${resumeBody.profileImage}`
				: null;

		const isFileExisting = fullPath ? await isPathReal(fullPath) : false;

		if (isFileExisting && fullPath) {
			await unlink(fullPath);
		}

		resumeBody.userId = req.user?._id;
		resumeBody.profileImage = null;

		await ClassicResumes.updateOneClassicResume(resumeBody);

		responseHandler(res, Result.ok());
		return;
	} catch (e) {
		handleCatch(res, e);
	}
};

export const deleteResume = async (
	req: CvBuilderRequestInterface<Partial<ClassicResumePreviewType>>,
	res: Response
): Promise<void> => {
	if (!req.body || !req.body._id || !req.body.userId) {
		responseHandler(res, Result.fail(`Wrong body or no body at all`, 400));
		return;
	}

	const resumePath = resumeFoldersPath(req.body._id as string, req.body.userId as string);
	const hasAssets = await isPathReal(resumePath);

	try {
		if (hasAssets) {
			await rm(resumePath, { recursive: true });
		}

		await ClassicResumes.removeOne({ _id: req.body._id });

		responseHandler(res, Result.ok());
		return;
	} catch (e) {
		handleCatch(res, e);
		return;
	}
};
