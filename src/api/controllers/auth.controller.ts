import { CookieOptions, Response } from 'express';
import { Users } from '../../schema';
import { NewUserResponseInterface, UserInterface } from '../../models';
import { JwtPayload, sign, verify } from 'jsonwebtoken';
import { hashString, revokeToken, unHashString, whiteListToken } from '../../utils/auth.utils';
import { CvBuilderRequestInterface } from '../../types/request.types';
import { callEmailApi, callMeApi, getAccessToken } from '../../utils/linkedin.utils';
import { responseHandler } from '../../utils/response.utils';
import { Result } from '../../utils/result.utils';
import { handleCatch } from '../../utils/controller.utils';
import { Mailer } from '../../utils/mail.utils';
import {
	CheckEmailExistPayloadInterface,
	GetLinkedinTokenPayloadInterface,
	SignInPayloadInterface,
	SignInResponseInterface,
	SignUpPayloadInterface,
	SignUpResponseInterface,
	VerifyEmailPayloadInterface
} from '../../types/auth.types';
import moment from 'moment';
import { LangType } from '../../types/lang.types';

const cookieOptions: CookieOptions = {
	domain: process.env.NODE_ENV === 'production' ? '.cv-template.co' : '.cv-template.xyz',
	expires: moment().add(7, 'days').toDate(),
	secure: process.env.NODE_ENV === 'production'
};

const currentTemplates = ['protea', 'liatris', 'zinnia', 'genista'];

export const signUp = async (req: CvBuilderRequestInterface<SignUpPayloadInterface>, res: Response): Promise<void> => {
	const { SECRET } = process.env;

	try {
		let codeName = 'zinnia';

		if (!req.body || !req.body.email || !req.body.name || !req.body.lastName) {
			responseHandler(res, Result.fail('One or more fields are missing!'));
			return;
		}

		if (req.body.codeName) {
			const queryCodeName = req.body.codeName.split('_')[0];
			codeName = currentTemplates.includes(queryCodeName) ? queryCodeName : 'zinnia';
		}

		const result = await Users.getOne({ email: req.body.email });

		if (result) {
			responseHandler<UserInterface>(
				res,
				Result.fail(
					`(user-already-exists) User corresponding to the email ${req.body.email} already exists!`,
					409
				)
			);
			return;
		}

		const user: NewUserResponseInterface = await Users.newUser(
			req.body,
			codeName,
			(req.headers['x-client-language'] || 'en') as LangType
		);
		const accessToken = sign({ user }, SECRET, { expiresIn: '7d' });
		const jwtPayload = verify(accessToken, SECRET) as JwtPayload;
		await whiteListToken(accessToken);
		const fatRes = res
			.cookie('handler_crow', accessToken, {
				...cookieOptions,
				httpOnly: true
			})
			.cookie('x-client-language', req.headers['x-client-language'], cookieOptions)
			.cookie('exp', jwtPayload.exp, cookieOptions);

		const mailer = new Mailer('welcome');
		await mailer.sendMail(
			user.email,
			mailer.recepientFullName(user),
			'Bienvenue à Cv-template.co!',
			mailer.getTemplate(
				{
					twitter: 'https://twitter.com/ProCVDesignr',
					pinterest: 'https://pinterest.fr/Cv-template.co',
					linkedin: 'https://linkedin.com/company/cv-template',
					howToCreateCV: 'https://cv-template.co/blog'
				},
				user
			)
		);

		responseHandler(
			fatRes,
			Result.ok<SignUpResponseInterface>({
				iat: jwtPayload.iat,
				exp: jwtPayload.exp,
				resumeLink: user.resumeLink
			})
		);
	} catch (e) {
		handleCatch(res, e);
	}
};

export const checkIfEmailExist = async (
	req: CvBuilderRequestInterface<CheckEmailExistPayloadInterface>,
	res: Response
) => {
	const result = await Users.getOne({ email: req.body.email });

	if (result) {
		responseHandler<boolean>(res, Result.ok(true));
		return;
	}

	responseHandler(res, Result.ok(false));
};

export const verifyEmail = async (
	req: CvBuilderRequestInterface<VerifyEmailPayloadInterface>,
	res: Response
): Promise<void> => {
	const { SECRET } = process.env;

	try {
		if (!req.body.key) {
			responseHandler(res, Result.fail('No key received', 400));
			return;
		}
		const { key } = req.body;
		const tempoToken = await unHashString(key);

		if (!tempoToken) {
			responseHandler(res, Result.fail('Wrong key', 400));
			return;
		}

		const payload = verify(tempoToken, SECRET) as JwtPayload;

		if (!payload || !payload.email) {
			responseHandler(res, Result.fail('Link expired', 410));
			return;
		}

		const user = await Users.getOne(payload as { email: string });

		if (!user) {
			responseHandler(res, Result.fail('User not found', 404));
			return;
		}

		const accessToken = sign({ user }, SECRET, { expiresIn: '7d' });
		const jwtPayload = verify(accessToken, SECRET) as JwtPayload;
		await whiteListToken(accessToken);
		const fatRes = res
			.cookie('handler_crow', accessToken, {
				...cookieOptions,
				httpOnly: true
			})
			.cookie('x-client-language', req.headers['x-client-language'], cookieOptions)
			.cookie('exp', jwtPayload.exp, cookieOptions);
		responseHandler(
			fatRes,
			Result.ok<SignInResponseInterface>({
				iat: jwtPayload.iat,
				exp: jwtPayload.exp
			})
		);
		return;
	} catch (e) {
		handleCatch(res, e);
	}
};

export const signIn = async (req: CvBuilderRequestInterface<SignInPayloadInterface>, res: Response): Promise<void> => {
	const { SECRET } = process.env;

	try {
		if (!req.body.email) {
			responseHandler(res, Result.fail('You need the email to sign-in!'));
			return;
		}

		const user = await Users.getOne(req.body);

		if (!user) {
			responseHandler<UserInterface>(
				res,
				Result.fail(
					`(user-do-not-exists) User corresponding to the email ${req.body.email} doesn't exist!`,
					409
				)
			);
			return;
		}

		const temporaryToken: string = sign({ email: user.email }, SECRET, { expiresIn: '30m' });
		const tokenForEmail = await hashString(temporaryToken);

		const mailer = new Mailer('auth');
		await mailer.sendMail(
			user.email,
			mailer.recepientFullName(user),
			'Lien magique!',
			mailer.getTemplate(
				{
					twitter: 'https://twitter.com/ProCVDesignr',
					pinterest: 'https://www.pinterest.fr/Cv-template.co',
					linkedin: 'https://www.linkedin.com/company/cv-template',
					magicLink: `${
						process.env.NODE_ENV === 'production' ? 'https://cv-template.co' : 'https://www.cv-template.xyz'
					}/?magic_link=${tokenForEmail}`
				},
				user
			)
		);

		responseHandler(res, Result.ok());
	} catch (e) {
		handleCatch(res, e);
	}
};

export const signOut = async (req: CvBuilderRequestInterface, res: Response): Promise<void> => {
	await revokeToken(req.cookies.handler_crow || '');
	responseHandler(
		res
			.cookie('handler_crow', '', {
				...{ ...cookieOptions, expires: new Date(1979, 1, 1) },
				httpOnly: true
			})
			.cookie('exp', '', { ...cookieOptions, expires: new Date(1979, 1, 1) }),
		Result.ok()
	);
	return;
};

export const setLanguage = (req: CvBuilderRequestInterface, res: Response): void => {
	responseHandler(
		res.cookie('x-client-language', req.headers['x-client-language'] || 'en', cookieOptions),
		Result.ok()
	);
	return;
};

export const getInfoFromLinkedIn = async (
	req: CvBuilderRequestInterface<GetLinkedinTokenPayloadInterface>,
	res: Response
): Promise<void> => {
	const accessToken = await getAccessToken(res, req.body.code, req.body.signUp);
	if (!accessToken) {
		return;
	}

	const meResult = await callMeApi(res, accessToken);
	const emailResult = await callEmailApi(res, accessToken);

	if (meResult && emailResult) {
		responseHandler(
			res,
			Result.ok({
				name: meResult.firstName,
				lastName: meResult.lastName,
				email: emailResult.email
			})
		);
	}

	return;
};
