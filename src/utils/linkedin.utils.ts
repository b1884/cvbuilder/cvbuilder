import { Response } from 'express';
import axios from 'axios';
import { handleCatch } from './controller.utils';

export const getAccessToken = async (res: Response, code: string, signUp: boolean): Promise<string> => {
	const { LINKEDIN_ACCESS_TOKEN_URL, LINKEDIN_CLIENT_SECRET, LINKEDIN_CLIENT_ID } = process.env;
	const url = signUp ? '/auth/cvbuilder' : '/auth/cvmaker';
	try {
		const linkedinAccessToken = await axios.post(LINKEDIN_ACCESS_TOKEN_URL, null, {
			params: {
				code,
				redirect_uri:
					process.env.NODE_ENV === 'production'
						? `https://cv-template.co${url}`
						: `https://www.cv-template.xyz${url}`,
				client_id: LINKEDIN_CLIENT_ID,
				client_secret: LINKEDIN_CLIENT_SECRET,
				grant_type: 'authorization_code'
			}
		});
		return linkedinAccessToken.data.access_token as string;
	} catch (e) {
		handleCatch(res, e);
	}
};

export const callMeApi = (
	res: Response,
	accessToken: string
): Promise<void | { firstName: string; lastName: string }> =>
	axios
		.get(process.env.LINKEDIN_ME_URL, {
			headers: { Authorization: `Bearer ${accessToken}` }
		})
		.then((result) => ({
			firstName: result.data.localizedFirstName,
			lastName: result.data.localizedLastName
		}));

export const callEmailApi = (res: Response, accessToken: string): Promise<void | { email: string }> =>
	axios
		.get(process.env.LINKEDIN_EMAIL_URL, {
			headers: { Authorization: `Bearer ${accessToken}` }
		})
		.then((result) => ({
			email: result.data.elements[0]['handle~'].emailAddress
		}));
