import { createHash } from 'crypto';
import { CvBuilderGlobalType } from '../types/cv-builder-global.types';
import { RedisClientType } from 'redis';

const whiteListedTokensKey = 'white_listed_tokens';

const getWhiteListedTokens = async (): Promise<string[]> => {
	const redisClient: RedisClientType | undefined = (<CvBuilderGlobalType>global).redisClient;
	if (!redisClient) {
		return [];
	}

	const tokensAsString = await redisClient.get(whiteListedTokensKey);

	if (!tokensAsString) {
		return [];
	}

	return JSON.parse(tokensAsString) as string[];
};

export const hashString = async (text: string): Promise<string> => {
	const redisClient: RedisClientType | undefined = (<CvBuilderGlobalType>global).redisClient;
	if (!redisClient) {
		return '';
	}

	const key = createHash('md5').update(text).digest('hex');

	await redisClient.set(key, text);
	return key;
};

export const unHashString = async (key: string): Promise<string | null> => {
	const redisClient: RedisClientType | undefined = (<CvBuilderGlobalType>global).redisClient;
	if (!redisClient) {
		return null;
	}

	return await redisClient.get(key);
};

export const deleteString = async (key: string): Promise<void> => {
	const redisClient: RedisClientType | undefined = (<CvBuilderGlobalType>global).redisClient;
	if (!redisClient) {
		return null;
	}

	await redisClient.del(key);
};

export const whiteListToken = async (token: string): Promise<void> => {
	const redisClient: RedisClientType | undefined = (<CvBuilderGlobalType>global).redisClient;
	if (!redisClient) {
		return;
	}

	const tokens = await getWhiteListedTokens();
	const newTokensArray = [...tokens, token];

	await redisClient.set(whiteListedTokensKey, JSON.stringify(newTokensArray));
};

export const revokeToken = async (token: string): Promise<void> => {
	const redisClient: RedisClientType | undefined = (<CvBuilderGlobalType>global).redisClient;
	const tokens = await getWhiteListedTokens();
	const tokenIndex = tokens.indexOf(token);

	if (tokenIndex < 0) {
		return;
	}

	tokens.splice(tokenIndex, 1);

	await redisClient.set(whiteListedTokensKey, JSON.stringify(tokens));
};

export const isTokenWhitelisted = async (token: string): Promise<boolean> => {
	const tokens = await getWhiteListedTokens();
	return tokens.includes(token);
};
