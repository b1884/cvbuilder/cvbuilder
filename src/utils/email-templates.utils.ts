import { UserInterface } from '../models';

export const activationEmailFR = (user: UserInterface, links: { [key: string]: string }) => `
<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <title>Welcome</title>
</head>
<body>
<h1>Connectez-vous à Cv-template.co ! </h1>
<p>
    Salut ${user.name},
    <br>
    Pour vous connecter à votre compte, cliquez sur le bouton ci-dessous :
</p>
<a href='${links.magicLink}' target='_blank' class='create-cv-button'>Connectez-vous à Cv-template.co !</a>
<p>
    Si le lien ne fonctionne pas, copiez cette URL dans votre navigateur : <a href='${links.magicLink}' target='_blank'>${links.magicLink}</a>.

</p>
</body>
</html>
`;

export const welcomeEmailFR = (user: UserInterface, links: { [key: string]: string }) => `
<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <title>Welcome</title>
</head>
<body>
<h1>Bienvenue ${user.name}!</h1>
<p>
    Salut ${user.name},
    <br>
    Bienvenue à Cv-template.co - une communauté de demandeurs d'emploi très pro.
    <br>
    Vous pouvez toujours accéder à votre compte en utilisant votre email pour vous connecter.
</p>
<p>
    <b>Détails du compte:</b><br>
    E-mail: ${user.email}
</p>
<p>
    Si vous cherchez à créer un excellent CV pour être embauché rapidement, vous êtes entre de bonnes mains.
</p>
<a href='https://cv-template.co' target='_blank' class='create-cv-button'>Créez votre meilleur CV !</a>
<p>
    Si vous voulez savoir comment écrire un CV professionnel et attirant, visitez cette <a href='${links.howToCreateCV}' target='_blank'>page</a>.
    <br>
    <br>
    À votre réussite,
    <br>
    <br>
    <span>L'équipe Cv-template.co.</span>
</p>
</body>
</html>
`;

export const verifyNewEmailFR = (user: UserInterface, links: { [key: string]: string }) => `
<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <title>Welcome</title>
</head>
<body>
<h1>Confirmer l’e-mail</h1>
<p>
    Bonjour ${user.name},
    <br>
    Nous avons reçu une demande de modification de l'adresse e-mail de votre compte sur cv-template.co.
    <br>
    Si vous avez demandé ce changement, veuillez le confirmer en cliquant sur le lien ci-dessous :
</p>
<a href='${links.verifyEmail}' target='_blank' class='create-cv-button'>Confirmer l’e-mail</a>
<p>
    Une fois que nous aurons reçu votre confirmation, nous mettrons à jour notre base de données avec votre nouvelle
    adresse e-mail.
</p>
<p>
    Après confirmation, vous pouvez utiliser votre nouvelle adresse e-mail pour vous connecter à votre compte actuel.
    Tous les futurs e-mails seront envoyés à cette nouvelle adresse.
</p>
<p>
    <b>Important</b> : Si vous n'avez pas demandé à changer l'adresse e-mail de votre compte, veuillez nous contacter immédiatement en répondant à cet e-mail.
</p>
<p>
    Si vous avez des questions, n'hésitez pas à nous les poser. Contactez-nous sur <a href='mailto:hi@cv-template.co'>cet e-mail</a> et nous serons heureux de vous aider.
    <br>
    <br>
    À votre réussite,
    <br>
    <br>
    <span>L'équipe Cv-template.co.</span>
</p>
</body>
</html>
`;

export const accountDeletedEmailFR = (user: UserInterface) => `
<!DOCTYPE html>
<html lang='fr'>
<head>
    <meta charset='UTF-8'>
    <title>Welcome</title>
</head>
<body>
<h1>Votre compte a été supprimé 😢</h1>
<p>
    Salut ${user.name},
</p>
<p>
    Votre compte, y compris toutes vos informations personnelles, a été supprimé à votre demande.
</p>
<p>
    Si vous étiez abonné à notre service premium, votre abonnement aura été annulé et aucune autre facturation ne sera effectuée.
</p>
<p>
    Vous ne recevrez pas non plus d'autres courriels de notre part après celui-ci.
</p>
<p>
    Bonne chance dans votre recherche d'emploi !
</p>
<p>
    À votre réussite,
    <br>
    <br>
    <span>L'équipe Cv-template.co.</span>
</p>
</body>
</html>
`;
