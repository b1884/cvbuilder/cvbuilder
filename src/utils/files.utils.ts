import { homedir } from 'os';
import { access, mkdir, readdir, stat, unlink } from 'fs/promises';
import { constants, readFileSync, Stats } from 'fs';
import { TemplateFileStatsInterface } from '../types/files.types';
import { CvBuilderGlobalType } from '../types/cv-builder-global.types';
import { LangType } from '../types/lang.types';
import { extname } from 'path';

const APP_ROOT = (<CvBuilderGlobalType>global).__appRoot;

export const resumesFolderName = 'cvbuilder_classic_resumes';
export const personalPhotosFolderName = 'users_personal_photos';
export const templatesPreviewsFolderName = 'cvbuilder_classic_templates';
export const templatePostFix = process.env.NODE_ENV === 'production' ? '.template.js' : '.template.ts';

export const templatesFolderRoot = `${process.cwd()}${
	process.env.NODE_ENV === 'production' ? '/dist' : ''
}/resume-templates`;
export const staticsRoot = `${homedir()}/.cvbuilderapp_static`;

export const templatesAssetsRoot = `${APP_ROOT}/templates-assets`;

export const templatesPreviewRoot = `${staticsRoot}/${templatesPreviewsFolderName}`;
export const mediaRoot = `${staticsRoot}/${resumesFolderName}`;
export const personalPhotosRoot = `${staticsRoot}/${personalPhotosFolderName}`;

export const templatePreviewXlFolderPath = (codeName: string, lang: LangType): string =>
	`${templatesPreviewRoot}/${codeName}/${lang}/xl`;
export const templatePreviewSmFolderPath = (codeName: string, lang: LangType): string =>
	`${templatesPreviewRoot}/${codeName}/${lang}/sm`;
export const templatePath = (codeName: string): string => `${templatesFolderRoot}/${codeName}${templatePostFix}`;

export const resumePdfFolderPath = (resumeId: string, userId: string): string =>
	`${mediaRoot}/${userId}/${resumeId}/pdf`;
export const resumeImagesFolderPath = (resumeId: string, userId: string): string =>
	`${mediaRoot}/${userId}/${resumeId}/images`;
export const resumeFoldersPath = (resumeId: string, userId: string): string => `${mediaRoot}/${userId}/${resumeId}`;

export const personalPhotosFolderPath = (userId: string): string => `${personalPhotosRoot}/${userId}`;

export const getClassicTemplateData = async (fileName: string): Promise<TemplateFileStatsInterface> => {
	const codeName = fileName.replace(templatePostFix, '');
	const name = codeName.charAt(0).toUpperCase() + codeName.slice(1);
	const path = `${templatesFolderRoot}/${fileName}`;
	const stats: Stats = await stat(path);

	return {
		codeName,
		name,
		path,
		lastUpdateDate: new Date(stats.mtime),
		creationDate: new Date(stats.birthtime)
	};
};

export const isPathReal = async (path: string): Promise<boolean> => {
	try {
		await access(path, constants.F_OK | constants.R_OK | constants.W_OK);
		return true;
	} catch {
		return false;
	}
};

export const makeClassicResumeFolders = async (resumeId: string, userId: string): Promise<void> => {
	const pdfFolder = resumePdfFolderPath(resumeId, userId);
	const imagesFolder = resumeImagesFolderPath(resumeId, userId);
	const personalPhotosFolder = personalPhotosFolderPath(userId);

	const canAccessPdfFolder = await isPathReal(pdfFolder);
	const canAccessImagesFolder = await isPathReal(imagesFolder);
	const canAccessPersonalPhotosFolder = await isPathReal(personalPhotosFolder);

	if (canAccessImagesFolder && canAccessPdfFolder && canAccessPersonalPhotosFolder) {
		return;
	}

	if (!canAccessImagesFolder) {
		await mkdir(imagesFolder, {
			recursive: true
		});
	}

	if (!canAccessPdfFolder) {
		await mkdir(pdfFolder, {
			recursive: true
		});
	}

	if (!canAccessPersonalPhotosFolder) {
		await mkdir(personalPhotosFolder, {
			recursive: true
		});
	}

	return;
};

export const makeTemplatesFolders = async (codeName: string, lang: LangType): Promise<void> => {
	if (!codeName || !codeName.length) {
		throw new Error('You should include a valid codeName!');
	}

	const xlFolder = templatePreviewXlFolderPath(codeName, lang);
	const smFolder = templatePreviewSmFolderPath(codeName, lang);

	const canAccessXlFolder = await isPathReal(xlFolder);
	const canAccessSmFolder = await isPathReal(smFolder);

	if (canAccessSmFolder && canAccessXlFolder) {
		return;
	}

	if (!canAccessSmFolder) {
		await mkdir(smFolder, {
			recursive: true
		});
	}

	if (!canAccessXlFolder) {
		await mkdir(xlFolder, {
			recursive: true
		});
	}

	return;
};

export const formatBytes = (bytes: number, decimals = 2): string => {
	if (bytes === 0) return '0 Bytes';

	const k = 1024;
	const dm = decimals < 0 ? 0 : decimals;
	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

	const i = Math.floor(Math.log(bytes) / Math.log(k));

	return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};

export const emptyMakeDir = async (path: string): Promise<void> => {
	const isFolderExisting = await isPathReal(path);

	if (!isFolderExisting) {
		await mkdir(path, { recursive: true });
		return;
	}

	const dirContent = await readdir(path);

	if (!dirContent || !dirContent.length) {
		return;
	}

	for (const file of dirContent) {
		await unlink(`${path}/${file}`);
	}
};

export const getImageDataURL = (path: string): string => {
	const ext = extname(path).split('.')[1];
	const urlPostFix = `data:image/${ext};base64,`;

	const imageAsBase64 = readFileSync(path, { encoding: 'base64' });

	return `${urlPostFix}${imageAsBase64}`;
};
