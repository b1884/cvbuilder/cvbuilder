import { isPathReal, templatePostFix, templatesFolderRoot } from './files.utils';

export const getTemplatePath = async (codename: string): Promise<string | null> => {
	const fullPath = `${templatesFolderRoot}/${codename}${templatePostFix}`;
	const canVisitPath = await isPathReal(fullPath);
	if (!canVisitPath) {
		return null;
	}

	return fullPath;
};
