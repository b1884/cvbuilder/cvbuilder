import { UserInterface } from '../models';
import { activationEmailFR, welcomeEmailFR, accountDeletedEmailFR, verifyNewEmailFR } from './email-templates.utils';
import {
	TransactionalEmailsApi,
	TransactionalEmailsApiApiKeys
} from 'sib-api-v3-typescript/api/transactionalEmailsApi';
import { SendSmtpEmail } from 'sib-api-v3-typescript/model/sendSmtpEmail';
import { CreateSmtpEmail } from 'sib-api-v3-typescript/model/createSmtpEmail';
import { IncomingMessage } from 'http';

export class Mailer {
	private readonly _companyEmail: string;
	private readonly _companyName: string;
	private readonly _apiKey: string;
	private readonly _apiKeyHeader: string;
	private readonly _emailHost: string;
	private readonly _type: 'auth' | 'welcome' | 'verifyEmail' | 'deleted';
	private readonly _apiInstance: TransactionalEmailsApi;

	constructor(type: 'auth' | 'welcome' | 'verifyEmail' | 'deleted') {
		const { MAIL_HOST, MAIL_NAME, MAIL_API_KEY, MAIL_API_KEY_HEADER, ...others } = process.env;
		this._apiKey = MAIL_API_KEY as string;
		this._apiKeyHeader = MAIL_API_KEY_HEADER as string;
		this._companyEmail = Mailer._chooseEmail(type, others);
		this._emailHost = MAIL_HOST as string;
		this._companyName = MAIL_NAME as string;
		this._type = type;
		this._apiInstance = new TransactionalEmailsApi();

		console.error(MAIL_HOST, MAIL_NAME, MAIL_API_KEY, MAIL_API_KEY_HEADER);
		this._apiInstance.setApiKey(TransactionalEmailsApiApiKeys.apiKey, MAIL_API_KEY as string);
	}

	private static _chooseEmail(
		type: 'auth' | 'welcome' | 'verifyEmail' | 'deleted',
		emails: { [key: string]: string | undefined }
	): string {
		const { MAIL_FROM_AUTH, MAIL_FROM_WELCOME, MAIL_FROM } = emails;

		if (!MAIL_FROM_AUTH || !MAIL_FROM_WELCOME || !MAIL_FROM) {
			throw new Error('Please set up the environment badii com on!!');
		}

		if (type === 'deleted' || type === 'welcome') {
			return MAIL_FROM_WELCOME;
		} else {
			return MAIL_FROM_AUTH;
		}
	}

	public getTemplate(links: { [key: string]: string }, user: UserInterface) {
		if (!user && this._type === 'welcome') {
			throw new Error('Please provide user');
		}

		if (this._type === 'auth') {
			return activationEmailFR(user, links);
		} else if (this._type === 'welcome') {
			return welcomeEmailFR(user, links);
		} else if (this._type === 'verifyEmail') {
			return verifyNewEmailFR(user, links);
		} else if (this._type === 'deleted') {
			return accountDeletedEmailFR(user);
		}
	}

	public recepientFullName(user: UserInterface): string {
		return `${user.name} ${user.lastName}`;
	}

	public async sendMail(
		recipientEmail: string,
		recipientName: string,
		subject: string,
		html?: string,
		text?: string
	): Promise<{ response: IncomingMessage; body: CreateSmtpEmail }> {
		const send: SendSmtpEmail = new SendSmtpEmail();

		send.to = [{ email: recipientEmail, name: recipientName }];
		send.sender = { email: this._companyEmail, name: this._companyName };
		send.subject = subject;
		send.htmlContent = html;
		send.textContent = text;

		return await this._apiInstance.sendTransacEmail(send);
	}
}
