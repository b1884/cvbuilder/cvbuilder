import { UniqueLinksType } from '../types/resume.types';
import { ClassicResumes } from '../schema';
import { UserInterface } from '../models';
import { ResumeInputInterface } from '../types/resume-input.types';

const characters = '0123456789abcdefghijklmnopqrstuvwxyz-_';
const linkLength = 10;
const sharingLinkLength = 6;

export const generateNewUserRawData = (user: UserInterface): ResumeInputInterface => ({
	title: 'builder.untitled',
	language: 'en',
	fields: [
		{
			title: 'builder.sections.personalInfo.title',
			firstName: user.name,
			lastName: user.lastName,
			email: user.email,
			code: 'personal-details'
		}
	]
});

const generateLink = (textLength: number): string => {
	const phrase = {
		text: ''
	};

	for (let i = 0; i <= textLength; i++) {
		const randomNumber = Math.floor(Math.random() * characters.length);
		phrase.text += characters.substring(randomNumber, randomNumber + 1);
	}

	return phrase.text;
};

const makeUniqueLink = async (): Promise<string> => {
	const link = generateLink(linkLength);
	const resume = await ClassicResumes.getMany({ link });

	if (!resume || !resume.length) {
		return link;
	}
	return await makeUniqueLink();
};

const makeUniqueSharingLink = async (): Promise<string> => {
	const sharingLink = generateLink(sharingLinkLength);
	const resume = await ClassicResumes.getMany({ sharingLink });

	if (!resume || !resume.length) {
		return sharingLink;
	}
	return await makeUniqueSharingLink();
};

export const generateUniqueLinks = async (): Promise<UniqueLinksType> => ({
	link: await makeUniqueLink(),
	sharingLink: await makeUniqueSharingLink()
});
