import { extname } from 'path';
import multer, { diskStorage, FileFilterCallback } from 'multer';
import { formatBytes, personalPhotosFolderPath } from './files.utils';
import { Request } from 'express';
import { CvBuilderRequestInterface } from '../types/request.types';
import Jimp, { AUTO, read } from 'jimp';

export const LEGAL_SIZE = 5242880; // We only 5megabytes or less.

const imageStorage = diskStorage({
	destination: (req, file, cb) => {
		if (!(<CvBuilderRequestInterface>req).user) {
			cb(new Error('No user'), '');
		}
		const id = (<CvBuilderRequestInterface>req)?.user?._id as string;
		cb(null, personalPhotosFolderPath(id));
	},
	filename: (req, file, cb) => {
		cb(null, `classic-resume-avatar_${Date.now()}${extname(file.originalname)}`);
	}
});

export const uploadImage = multer({
	limits: {
		files: 1,
		fileSize: LEGAL_SIZE
	},
	storage: imageStorage,
	fileFilter: (req: Request, file: Express.Multer.File, callback: FileFilterCallback) => {
		if (file.size && file.size > LEGAL_SIZE) {
			return callback(
				new Error(`File size exceeds allowed size! ( ${formatBytes(file.size)} > ${formatBytes(LEGAL_SIZE)} )`)
			);
		}

		if (file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'image/png') {
			return callback(
				new Error(
					`Uploaded file type isn't allowed! please consider uploading one of these file types [jpeg / jpg / png]`
				)
			);
		}

		return callback(null, true);
	}
}).single('file');

export const resizeImage = async (file: Express.Multer.File): Promise<string> => {
	const image: Jimp = await read(file.path);

	let imageWidth = image.getWidth();
	let imageHeight = image.getHeight();

	if (imageWidth < imageHeight) {
		imageWidth = 400;
		imageHeight = AUTO;
	} else {
		imageHeight = 400;
		imageWidth = AUTO;
	}

	await image.resize(imageWidth, imageHeight).quality(40).writeAsync(file.path);

	return file.filename;
};
