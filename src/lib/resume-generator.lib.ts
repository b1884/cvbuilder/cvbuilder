import { TemplateExecutor } from 'lodash';
import { emptyDir } from 'fs-extra';
import translations from '../locals';

import puppeteer, { PDFOptions } from 'puppeteer';
import { get } from 'lodash';
import { fromBuffer } from 'pdf2pic';
import { Options } from 'pdf2pic/dist/types/options';
import { Convert } from 'pdf2pic/dist/types/convert';
import { WriteImageResponse } from 'pdf2pic/dist/types/writeImageResponse';
import { FieldInterface, FieldValueInterface, ResumeInputInterface, SlotInterface } from '../types/resume-input.types';
import { LangType } from '../types/lang.types';
import {
	getImageDataURL,
	personalPhotosRoot,
	templatePreviewSmFolderPath,
	templatePreviewXlFolderPath,
	templatesAssetsRoot
} from '../utils/files.utils';
import { PreviewLinksType } from '../types/template.types';
import momentLibrary from 'moment';

const defPdfOptions: puppeteer.PDFOptions = {
	width: 1240,
	height: 1752,
	scale: 0.999,
	preferCSSPageSize: false,
	displayHeaderFooter: false,
	printBackground: true
};

const defImageOptions: Options = {
	density: 150,
	quality: 55,
	saveFilename: '',
	savePath: '',
	format: 'jpeg'
};

const xlDefImageOptions: Options = {
	...defImageOptions,
	width: 900,
	height: 1272
};

const smDefImageOptions: Options = {
	...defImageOptions,
	width: 308,
	height: 433.78
};

const minimalArgs = [
	'--autoplay-policy=user-gesture-required',
	'--disable-background-networking',
	'--disable-background-timer-throttling',
	'--disable-backgrounding-occluded-windows',
	'--disable-breakpad',
	'--disable-client-side-phishing-detection',
	'--disable-component-update',
	'--disable-default-apps',
	'--disable-dev-shm-usage',
	'--disable-domain-reliability',
	'--disable-extensions',
	'--disable-features=AudioServiceOutOfProcess',
	'--disable-hang-monitor',
	'--disable-ipc-flooding-protection',
	'--disable-notifications',
	'--disable-offer-store-unmasked-wallet-cards',
	'--disable-popup-blocking',
	'--disable-print-preview',
	'--disable-prompt-on-repost',
	'--disable-renderer-backgrounding',
	'--disable-setuid-sandbox',
	'--disable-speech-api',
	'--disable-sync',
	'--hide-scrollbars',
	'--ignore-gpu-blacklist',
	'--metrics-recording-only',
	'--mute-audio',
	'--no-default-browser-check',
	'--no-first-run',
	'--no-pings',
	'--no-sandbox',
	'--no-zygote',
	'--password-store=basic',
	'--use-gl=swiftshader',
	'--use-mock-keychain',
	`--window-size=${1240},${1752}`
];

export interface ResumeGeneratorOutputInterface {
	pages: number;
	previousPagesNumber: number;
	pagesNumberChanged: boolean;
	pdfOutput: string;
	previewImage: string;
	pagesImageOutput: WriteImageResponse[];
}

export interface ClassicGeneratorOutputInterface {
	previewImage: string;
}

export class ResumeGenerator {
	private readonly _imagesOutputPath!: string;
	private _resumeName!: string;

	private readonly _puppeteerOptions!: puppeteer.PDFOptions;
	private readonly _pdf2picOptions!: Options;

	private _templatePath = '';
	private _lang: LangType = 'fr';
	private _previousTemplatePath = '';
	private _dataForm!: ResumeInputInterface;
	private _template!: string;
	private _rawHtml!: TemplateExecutor;
	private _outPut!: ResumeGeneratorOutputInterface;

	constructor(resumeName: string, imagesOutputPath?: string) {
		this._imagesOutputPath = imagesOutputPath ? imagesOutputPath : '';
		this._resumeName = resumeName;

		this._puppeteerOptions = {
			...defPdfOptions
		};

		this._pdf2picOptions = {
			...xlDefImageOptions,
			saveFilename: resumeName,
			savePath: imagesOutputPath
		};
	}

	private _parseInput(): void {
		const t: typeof translations.fr =
			translations[<keyof typeof translations>(this.lang || this._dataForm.language)];
		const moment = momentLibrary().locale(this.lang);

		if (this._dataForm.profileImage && this._dataForm.profileImage.includes('templates_images')) {
			this._dataForm.profileImage = getImageDataURL(`${templatesAssetsRoot}/${this._dataForm.profileImage}`);
		}

		if (this._dataForm.profileImage && this._dataForm.profileImage.includes('classic-resume-avatar')) {
			this._dataForm.profileImage = getImageDataURL(
				`${personalPhotosRoot}/${this._dataForm.profileImage.replace('personal_photos/', '')}`
			);
		}

		if (this._dataForm.title.includes('builder.')) {
			this._dataForm.title = t.builder.untitled;
		}

		if (this._dataForm.fields && this._dataForm.fields.length) {
			this._dataForm.fields = this._dataForm.fields.map((field: FieldInterface) => {
				if (field.code === 'personal-details') {
					const canShowAddress = !!(field.city || field.address || field.zipCode || field.country);

					Object.keys(field).forEach((key: string) => {
						if (
							key !== 'title' &&
							key !== 'code' &&
							key !== 'customTitle' &&
							field[key as keyof FieldInterface]
						) {
							field[key as keyof FieldInterface] = <FieldValueInterface>{
								label: get(t, `builder.sections.personalInfo.${key}.label`) as unknown as string,
								value: field[key as keyof FieldInterface]
							};
						}

						if (key === 'address' && field[key as keyof FieldInterface]) {
							field[key as keyof FieldInterface] = <FieldValueInterface>{
								...(<FieldValueInterface>field[key as keyof FieldInterface]),
								show: canShowAddress
							};
						}
					});
				}

				if (field.slots && field.slots.length) {
					if (field.code === 'languages' || field.code === 'skills') {
						field.slots = field.slots.map((slot) => ({
							...slot,
							levelInText: get(
								t,
								`builder.sections.${field.code}.level.${slot.level}`
							) as unknown as string
						}));
					}

					field.slots = field.slots.map((slot: SlotInterface) => {
						if (slot.durationEnd && slot.durationEnd.includes('builder.misc')) {
							slot.durationEnd = get(t, slot.durationEnd) as unknown as string;
						} else if (slot.durationEnd) {
							const date = new Date(slot.durationEnd);
							slot.durationEnd = moment
								.set({
									year: date.getFullYear(),
									month: date.getMonth()
								})
								.format('MMM YYYY');
						}

						if (slot.duration) {
							const date = new Date(slot.duration);
							slot.duration = moment
								.set({
									year: date.getFullYear(),
									month: date.getMonth()
								})
								.format('MMM YYYY');
						}
						return slot;
					});
				}

				Object.keys(field).forEach((key: string) => {
					if (key === 'title' && field[key].includes('builder.section')) {
						field[key] = get(t, field[key]) as string;
					}
				});

				return field;
			});
		}
	}

	private async _compileTemplate(): Promise<string> {
		this._rawHtml = await this._getHtmlFile();
		return this._rawHtml({ resume: this._dataForm });
	}

	private async _getHtmlFile(): Promise<TemplateExecutor> {
		return (await import(this._templatePath)).default as TemplateExecutor;
	}

	async generateTemplatePreviews(lang: LangType): Promise<PreviewLinksType> {
		this._template = await this._compileTemplate();

		const pdfBuffer = await this._generatePdf();

		await emptyDir(templatePreviewSmFolderPath(this._resumeName, lang));
		await emptyDir(templatePreviewXlFolderPath(this._resumeName, lang));

		const xlPreview = await this._generatePreviewImage(
			templatePreviewXlFolderPath(this._resumeName, lang),
			`xl_${this._resumeName}`,
			'xl',
			pdfBuffer
		);
		const smPreview = await this._generatePreviewImage(
			templatePreviewSmFolderPath(this._resumeName, lang),
			`sm_${this._resumeName}`,
			'sm',
			pdfBuffer
		);

		return {
			previewSm: `${this._resumeName}/${lang}/sm/${smPreview}`,
			previewXl: `${this._resumeName}/${lang}/xl/${xlPreview}`
		};
	}

	private async _generatePdf(path?: string): Promise<Buffer> {
		const browser = await puppeteer.launch({
			args: minimalArgs,
			headless: true,
			defaultViewport: {
				width: 1240,
				height: 1752,
				deviceScaleFactor: 1
			}
		});
		const page = await browser.newPage();
		const options: PDFOptions = Object.assign({}, this._puppeteerOptions);

		page.on('console', (event) => {
			if (event.type() === 'log') {
				console.log('page log = ', event.text());
			}
			if (event.type() === 'error') {
				console.log('page error = ', event.text());
			}
		});

		await page.setContent(this._template, {
			waitUntil: ['domcontentloaded', 'networkidle0', 'networkidle2', 'load']
		});
		await page.evaluateHandle('document.fonts.ready');

		if (path) {
			options.path = path;
		}
		const buffer = await page.pdf(options);
		await browser.close();

		return buffer;
	}

	async generateClassicResumePreview(): Promise<string> {
		this._template = await this._compileTemplate();

		const pdfBuffer = await this._generatePdf();

		await emptyDir(this._imagesOutputPath);

		return await this._generatePreviewImage(this._imagesOutputPath, `preview_${this._resumeName}`, 'sm', pdfBuffer);
	}

	async generatePDF(absolutePath: string): Promise<string> {
		const t: typeof translations.fr =
			translations[<keyof typeof translations>(this.lang || this._dataForm.language)];
		this._template = await this._compileTemplate();

		if (this._resumeName.includes('builder.')) {
			this._resumeName = get(t, this._resumeName) as unknown as string;
		}

		const fileName = `${this._resumeName}.pdf`;
		const pdfPath = `${absolutePath}/${fileName}`;
		await this._generatePdf(pdfPath);
		return fileName;
	}

	async generatePDFForBuilder(): Promise<Buffer> {
		this._template = await this._compileTemplate();
		return await this._generatePdf();
	}

	async generateResumeImages(pdfBuffer: Buffer): Promise<ClassicGeneratorOutputInterface> {
		const tempName = `${new Date().getTime()}`;

		await emptyDir(this._imagesOutputPath);

		const previewImage = await this._generatePreviewImage(
			this._imagesOutputPath,
			`preview_${tempName}`,
			'sm',
			pdfBuffer
		);
		this._pdf2picOptions.saveFilename = tempName;
		const convertToImages: Convert = fromBuffer(pdfBuffer, this._pdf2picOptions);

		if (!convertToImages.bulk) {
			throw new Error(`Can't convert images`);
		}

		await convertToImages.bulk(-1);

		return {
			previewImage
		};
	}

	private async _generatePreviewImage(
		savePath: string,
		fileName: string,
		size: 'xl' | 'sm' = 'sm',
		buffer: Buffer
	): Promise<string> {
		const defOptions = size === 'sm' ? smDefImageOptions : xlDefImageOptions;
		const previewPdf2picOptions: Options = {
			...defOptions,
			saveFilename: fileName,
			savePath
		};

		const generatePreview: Convert = fromBuffer(buffer, previewPdf2picOptions);

		const imageName = ((await generatePreview(1)) as WriteImageResponse).name;
		return imageName ? imageName : '';
	}

	set templatePath(value: string) {
		if (this._templatePath && value !== this._templatePath) {
			this._previousTemplatePath = this._templatePath;
		}

		if (!this._templatePath) {
			this._previousTemplatePath = value;
		}

		this._templatePath = value;
	}

	set dataForm(value: ResumeInputInterface) {
		this._dataForm = value;

		this._parseInput();
	}

	get dataForm(): ResumeInputInterface {
		return this._dataForm;
	}

	get outPut(): ResumeGeneratorOutputInterface {
		return this._outPut;
	}

	get lang(): LangType {
		return this._lang;
	}

	set lang(value: LangType) {
		this._lang = value;
	}
}
