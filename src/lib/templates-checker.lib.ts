import { readdir, readFile } from 'fs/promises';
import {
	getClassicTemplateData,
	makeTemplatesFolders,
	templatePostFix,
	templatesFolderRoot
} from '../utils/files.utils';
import { TemplateFileStatsInterface } from '../types/files.types';
import { TemplateInterface } from '../models';
import { Templates } from '../schema';
import { PreviewLinksType } from '../types/template.types';
import { ResumeGenerator } from './resume-generator.lib';
import { ResumeInputInterface } from '../types/resume-input.types';
import { LangType } from '../types/lang.types';
import { LANGUAGES } from '../config/enabled-languages.config';

export class TemplatesChecker {
	private _templatesFiles!: TemplateFileStatsInterface[];
	private _databaseTemplates!: TemplateInterface[];

	constructor() {
		this._databaseTemplates = [];
		this._templatesFiles = [];
	}

	get templatesFiles(): TemplateFileStatsInterface[] {
		return this._templatesFiles;
	}

	set templatesFiles(value: TemplateFileStatsInterface[]) {
		this._templatesFiles = value;
	}

	get databaseTemplates(): TemplateInterface[] {
		return this._databaseTemplates;
	}

	set databaseTemplates(value: TemplateInterface[]) {
		this._databaseTemplates = value;
	}

	async getTemplatesFiles() {
		const dirContent = await readdir(templatesFolderRoot);
		const templatesFilesNames = dirContent.filter((tempName: string) => tempName.includes(templatePostFix));
		const templatesInfo = templatesFilesNames.map((tempName: string) => getClassicTemplateData(tempName));

		this.templatesFiles = await Promise.all(templatesInfo);
	}

	async getTemplatesFromDB() {
		this.databaseTemplates = await Templates.getMany();
	}

	async getInput(codeName: string, lang: LangType): Promise<ResumeInputInterface> {
		const input = await readFile(`${templatesFolderRoot}/fillers/${codeName}.${lang}.filler.json`, {
			encoding: 'utf-8'
		});
		return JSON.parse(input) as ResumeInputInterface;
	}

	private async _generatePreviews(
		templateFile: TemplateFileStatsInterface,
		lang: LangType
	): Promise<PreviewLinksType> {
		const generator: ResumeGenerator = new ResumeGenerator(templateFile.codeName);
		generator.lang = lang;
		generator.dataForm = await this.getInput(templateFile.codeName, lang);
		generator.templatePath = templateFile.path;

		return await generator.generateTemplatePreviews(lang);
	}

	private async _addTemplate(templateFile: TemplateFileStatsInterface) {
		for (const lang of LANGUAGES as LangType[]) {
			await makeTemplatesFolders(templateFile.codeName, lang);
			const previews: PreviewLinksType = await this._generatePreviews(templateFile, lang);

			const template: TemplateInterface = {
				...previews,
				lang,
				name: templateFile.name,
				codeName: `${templateFile.codeName}_${lang}`,
				lastUpdateDate: templateFile.lastUpdateDate,
				creationDate: templateFile.creationDate
			};

			await Templates.newTemplate(template);
		}
	}

	private async _updateTemplate(templateFile: TemplateFileStatsInterface, _id: string) {
		for (const lang of LANGUAGES as LangType[]) {
			await makeTemplatesFolders(templateFile.codeName, lang);

			const previews: PreviewLinksType = await this._generatePreviews(templateFile, lang);

			const template: TemplateInterface = {
				...previews,
				lang: lang,
				name: templateFile.name,
				codeName: `${templateFile.codeName}_${lang}`,
				lastUpdateDate: templateFile.lastUpdateDate,
				creationDate: templateFile.creationDate,
				_id
			};

			await Templates.updateOneTemplate(template);
		}
	}

	async addNewTemplates() {
		const nonExistingTemplates: TemplateFileStatsInterface[] = this.templatesFiles.filter(
			(template: TemplateFileStatsInterface) =>
				-1 === this.databaseTemplates.findIndex((t) => t.codeName === template.codeName)
		);

		// quit the function if the templates' folder doesn't include
		// any template that doesn't exist in DB
		if (!nonExistingTemplates || !nonExistingTemplates.length) {
			return;
		}

		const templatesAdder = nonExistingTemplates.map((template) => this._addTemplate(template));

		await Promise.all(templatesAdder);
	}

	async removeNonExistingTemplates() {
		const nonExistingFileTemplates: TemplateInterface[] = this.databaseTemplates.filter(
			(template: TemplateInterface) =>
				-1 === this.templatesFiles.findIndex((t) => t.codeName === template.codeName)
		);

		if (!nonExistingFileTemplates || !nonExistingFileTemplates.length) {
			return;
		}

		const nonExistingFileTemplatesRemover = nonExistingFileTemplates.map((template: TemplateInterface) =>
			Templates.removeOne(template)
		);

		await Promise.all(nonExistingFileTemplatesRemover);
	}

	async updateExistingTemplate() {
		const updatedTemplates: TemplateFileStatsInterface[] = this.templatesFiles.filter(
			(template: TemplateFileStatsInterface) =>
				-1 ===
				this.databaseTemplates.findIndex(
					(t) =>
						t.codeName.includes(template.codeName) &&
						new Date(t.lastUpdateDate).getTime() === new Date(template.lastUpdateDate).getTime()
				)
		);

		// quit the function if the templates' folder doesn't include
		// any template that didn't get updated lately
		if (!updatedTemplates || !updatedTemplates.length) {
			return;
		}

		const templatesAdder = updatedTemplates.map((template) => {
			const dbArrIdx = this.databaseTemplates.findIndex(
				(dbTemp: TemplateInterface) => dbTemp.codeName === template.codeName
			);
			if (dbArrIdx < 0) {
				return;
			}

			const dbTemp = this.databaseTemplates[dbArrIdx];

			return this._updateTemplate(template, dbTemp._id);
		});

		await Promise.all(templatesAdder);
		console.info(
			`Templates files updated! : \n 🐚🐚 ${updatedTemplates.length} template files found and updated the version in DB.`
		);
	}
}
