import { Schema, model } from 'mongoose';
import {
	ClassicResumeInterface,
	NewUserResponseInterface,
	UserInterface,
	UserModelInterface,
	UserType
} from '../models';
import { ClassicResumes } from './classic-resume.schema';
import { generateNewUserRawData } from '../utils/resume.utils';
import { LangType } from '../types/lang.types';

const UserSchema = new Schema<UserInterface>({
	name: {
		type: Schema.Types.String,
		trim: true,
		minlength: 2,
		maxlength: 20,
		required: true
	},
	lastName: {
		type: Schema.Types.String,
		trim: true,
		minlength: 2,
		maxlength: 20,
		required: true
	},
	email: {
		type: Schema.Types.String,
		unique: true,
		trim: true,
		lowercase: true,
		required: true,
		match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
	},
	unConfirmedEmail: {
		type: Schema.Types.String,
		trim: true,
		lowercase: true,
		match: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
	},
	subscriptionType: {
		type: Schema.Types.String,
		trim: true
	},
	subscriptionStartTime: {
		type: Schema.Types.Date
	},
	creationDate: {
		type: Schema.Types.Date,
		default: Date.now()
	}
});

UserSchema.statics.newUser = async (
	body: UserInterface,
	templateCodeName?: string,
	lang?: LangType
): Promise<NewUserResponseInterface> => {
	if (process.env.IS_BETA_TESTING) {
		body.subscriptionType = '60-days-betaTestingGift';
		body.subscriptionStartTime = new Date();
	}

	const _user = new Users(body);
	const newUser = (await _user.save()).toJSON() as UserInterface;
	const newRawData = generateNewUserRawData(newUser);
	const firstResumeBody: Partial<ClassicResumeInterface> = {
		userId: newUser._id,
		lang: lang || newRawData.language,
		templateCodeName: templateCodeName ? templateCodeName : 'zinnia',
		title: newRawData.title,
		rawData: newRawData
	};
	const firstResume = await ClassicResumes.newClassicResume(firstResumeBody);

	return {
		...newUser,
		resumeLink: firstResume.link
	} as NewUserResponseInterface;
};

UserSchema.statics.getMany = async (query: Partial<UserInterface>): Promise<UserInterface[]> =>
	await Users.find(query).exec();

UserSchema.statics.getOne = async (query: Partial<UserInterface>): Promise<UserInterface | null> =>
	await Users.findOne(query).exec();

UserSchema.statics.updateOneUser = async (body: UserInterface): Promise<UserInterface | null> => {
	const { _id, ...others } = body;
	return await Users.findOneAndUpdate({ _id }, others, { new: true }).exec();
};

UserSchema.statics.removeOne = async (body: UserInterface): Promise<UserInterface | null> =>
	await Users.findByIdAndDelete(body._id).exec();

export const Users = model<UserType, UserModelInterface>('users', UserSchema);
