import { Schema, model, Types } from 'mongoose';
import {
	ClassicResumeInterface,
	ClassicResumeModelInterface,
	ClassicResumePreviewType,
	ClassicResumeType
} from '../models';
import { UniqueLinksType } from '../types/resume.types';
import { generateUniqueLinks } from '../utils/resume.utils';
import {
	makeClassicResumeFolders,
	mediaRoot,
	resumeImagesFolderPath,
	resumePdfFolderPath,
	templatePath
} from '../utils/files.utils';
import { ResumeGenerator } from '../lib/resume-generator.lib';

const ClassicResumeSchema = new Schema<ClassicResumeInterface>({
	userId: {
		type: Schema.Types.ObjectId,
		required: true
	},
	templateCodeName: {
		type: Schema.Types.String,
		trim: true,
		minlength: 2,
		maxlength: 60,
		required: true
	},
	title: {
		type: Schema.Types.String,
		trim: true,
		minlength: 2,
		maxlength: 60,
		required: true
	},
	lang: {
		type: Schema.Types.String,
		trim: true,
		minlength: 2,
		maxlength: 2,
		required: true,
		default: 'fr'
	},
	link: {
		type: Schema.Types.String,
		unique: true,
		trim: true,
		required: true
	},
	sharingLink: {
		type: Schema.Types.String,
		unique: true,
		trim: true,
		required: true
	},
	isShareable: {
		type: Schema.Types.Boolean
	},
	rawData: {
		type: Schema.Types.String,
		trim: true,
		minlength: 2,
		required: true
	},
	profileImage: {
		type: Schema.Types.String,
		trim: true
	},
	previewImage: {
		type: Schema.Types.String,
		trim: true
	},
	creationDate: {
		type: Schema.Types.Date,
		default: Date.now()
	},
	lastUpdateDate: {
		type: Schema.Types.Date,
		default: Date.now()
	}
});

ClassicResumeSchema.statics.newClassicResume = async (
	body: ClassicResumeInterface
): Promise<ClassicResumeInterface> => {
	const links: UniqueLinksType = await generateUniqueLinks();
	const resumeBody: ClassicResumeInterface = {
		...body,
		...links,
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		rawData: JSON.stringify(body.rawData) as any,
		_id: new Types.ObjectId()
	};

	const imagesFolder = resumeImagesFolderPath(
		(resumeBody._id as Types.ObjectId).toString(),
		resumeBody.userId as string
	);

	await makeClassicResumeFolders((resumeBody._id as Types.ObjectId).toString(), resumeBody.userId as string);

	const generator = new ResumeGenerator(resumeBody.link, imagesFolder);
	generator.templatePath = templatePath(resumeBody.templateCodeName);
	generator.lang = 'fr';
	generator.dataForm = Object.assign({}, body.rawData);

	resumeBody.previewImage = await generator.generateClassicResumePreview();

	const classicResume = new ClassicResumes(resumeBody);
	const result = (await classicResume.save()).toJSON();

	// eslint-disable-next-line @typescript-eslint/no-unsafe-argument,@typescript-eslint/no-unsafe-assignment
	return { ...result, rawData: JSON.parse((result as any).rawData) } as ClassicResumeInterface;
};

ClassicResumeSchema.statics.getPreviews = async (
	query: Partial<ClassicResumeInterface>
): Promise<ClassicResumePreviewType[]> => await ClassicResumes.find(query).select('-rawData').exec();

ClassicResumeSchema.statics.getMany = async (
	query: Partial<ClassicResumeInterface>
): Promise<ClassicResumeInterface[]> => await ClassicResumes.find(query).exec();

ClassicResumeSchema.statics.getOne = async (
	query: Partial<ClassicResumeInterface>
): Promise<ClassicResumeInterface | null> => {
	const doc = (await ClassicResumes.findOne(query).exec())?.toJSON();

	if (!doc) {
		return null;
	}

	// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-argument
	return { ...doc, rawData: JSON.parse((doc as any).rawData) } as ClassicResumeInterface;
};

ClassicResumeSchema.statics.updateOneClassicResume = async (
	body: Partial<ClassicResumeInterface>
): Promise<ClassicResumeInterface | null> => {
	const { _id, userId, ...others } = body;

	if (others.rawData && typeof others.rawData === 'object') {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		others.rawData = JSON.stringify(body.rawData) as any;
	}

	return (
		await ClassicResumes.findOneAndUpdate(
			{ _id, userId },
			{ ...others, lastUpdateDate: Date.now() },
			{ new: true }
		).exec()
	)?.toJSON() as ClassicResumeInterface | null;
};

ClassicResumeSchema.statics.removeOne = async (body: ClassicResumeInterface): Promise<ClassicResumeInterface | null> =>
	await ClassicResumes.findByIdAndDelete(body._id).exec();

ClassicResumeSchema.statics.removeAllByUserId = async (
	body: Pick<ClassicResumeInterface, 'userId'>
): Promise<ClassicResumeInterface | null> => await ClassicResumes.remove({ userId: body.userId }).exec();

export const ClassicResumes = model<ClassicResumeType, ClassicResumeModelInterface>(
	'classic_resume',
	ClassicResumeSchema
);
