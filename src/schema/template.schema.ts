import { Schema, model } from 'mongoose';
import { TemplateInterface, TemplateModelInterface, TemplateType } from '../models';

const TemplateSchema = new Schema<TemplateInterface>({
	name: {
		type: Schema.Types.String,
		trim: true,
		minlength: 2,
		maxlength: 20,
		required: true
	},
	codeName: {
		type: Schema.Types.String,
		trim: true,
		lowercase: true,
		unique: true,
		minlength: 2,
		maxlength: 20,
		required: true
	},
	previewSm: {
		type: Schema.Types.String,
		trim: true,
		required: true
	},
	previewXl: {
		type: Schema.Types.String,
		trim: true,
		required: true
	},
	lang: {
		type: Schema.Types.String,
		trim: true,
		required: true
	},
	lastUpdateDate: {
		type: Schema.Types.Date,
		default: Date.now()
	},
	creationDate: {
		type: Schema.Types.Date,
		default: Date.now()
	}
});

TemplateSchema.statics.newTemplate = async (body: TemplateInterface): Promise<TemplateInterface> => {
	const _template = new Templates(body);
	return await _template.save();
};

TemplateSchema.statics.getMany = async (query?: Partial<TemplateInterface>): Promise<TemplateInterface[]> =>
	await Templates.find(query ? query : {}).exec();

TemplateSchema.statics.getOne = async (query: Partial<TemplateInterface>): Promise<TemplateInterface | null> =>
	await Templates.findOne(query).exec();

TemplateSchema.statics.updateOneTemplate = async (body: TemplateInterface): Promise<TemplateInterface | null> => {
	const { _id, ...others } = body;
	return await Templates.findOneAndUpdate({ _id }, others, { new: true }).exec();
};

TemplateSchema.statics.removeOne = async (body: TemplateInterface): Promise<TemplateInterface | null> =>
	await Templates.findByIdAndDelete(body._id).exec();

export const Templates = model<TemplateType, TemplateModelInterface>('templates', TemplateSchema);
