import fr from './fr';
import en from './en';
import ar from './ar';

export default {
	fr,
	en,
	ar
};
