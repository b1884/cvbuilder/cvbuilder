export default {
	untitled: 'Untitled',
	misc: {
		toPresent: 'Present',
		worksHere: 'Currently work here'
	},
	addSection: 'Add a section',
	extraSections: {
		custom: 'Customized Section',
		courses: 'Courses',
		extraActivities: 'Extra-professional activities',
		trainings: 'Internships',
		fun: 'Leisure',
		languages: 'Languages'
	},
	additionalInfo: {
		show: 'Show additional information',
		hide: 'Hide additional information'
	},
	sections: {
		personalInfo: {
			title: 'Personal Information',
			postTitle: {
				label: 'Job Title',
				placeholder: 'e.g.: Teacher',
				hint: 'Add a title like "Senior Marketer" or "Sales Executive" that quickly describes your overall experience or the type of position you are applying for'
			},
			addProfileImage: {
				add: 'Download a photo',
				edit: 'Edit photo',
				remove: 'Delete photo'
			},
			lastName: {
				label: 'Family name'
			},
			firstName: {
				label: 'First name'
			},
			email: {
				label: 'E-mail'
			},
			phone: {
				label: 'Phone number'
			},
			country: {
				label: 'Country'
			},
			city: {
				label: 'City'
			},
			address: {
				label: 'Address'
			},
			zipCode: {
				label: 'Zip code'
			},
			drivingLicense: {
				label: 'Driving License',
				hint: 'Include this section if your occupation requires a certain type of license. If not, leave it blank.'
			},
			nationality: {
				label: 'Nationality',
				hint: 'Indicate your nationality only if it is relevant to the position offered. In most cases, leave this field blank.'
			},
			birthLocation: {
				label: 'Birth Location',
				hint: 'Only add your birth location if it is a requirement for the offered position. In most cases, leave this field blank.'
			},
			birthDate: {
				label: 'Birth date',
				hint: 'Only add your date of birth if it is a requirement for the offered position. In most cases, leave this field blank.'
			}
		},
		personalResume: {
			title: 'Personal Resume',
			sectionDescription: 'Include 2 or 3 clear sentences about your overall experience and core skills.',
			placeholder:
				'e.g. A passionate web developer with more than 8 years of experience and a perfect mastery of Python programming language ...'
		},
		employmentHistory: {
			title: 'Professional experience',
			sectionDescription:
				'Include the most relevant work experience and dates in this section. List the most recent positions first.',
			addSlot: 'Add a job',
			postTitle: {
				label: 'Job Title'
			},
			host: {
				label: 'Employer'
			},
			duration: {
				label: 'Start and End Date',
				hint: 'If you do not want to display the month, you can type the year only. Leave the field blank if you do not want to display the dates for this entry. <br /> Example : « March 2016 », « 2016 » or empty'
			},
			city: {
				label: 'City'
			},
			description: {
				label: 'Description',
				placeholder:
					'e.g., creating and implementing lesson plans based on children interests and curiosities.',
				tip: 'Tip: write a 200+ character text that includes mainly the most relevant keywords for the offered position.'
			}
		},
		educationHistory: {
			title: 'Education',
			sectionDescription: 'Indicate your most recent academic training, degrees, and results here',
			addSlot: 'Add a course',
			postTitle: {
				label: 'Degree'
			},
			host: {
				label: 'School'
			},
			duration: {
				label: 'Start and End Date',
				hint: 'If you do not want to display the month, you can type the year only. Leave the field blank if you do not want to display the dates for this entry. <br /> Example : « March 2016 », « 2016 » or empty'
			},
			city: {
				label: 'City'
			},
			description: {
				label: 'Description',
				placeholder: 'For example, graduated with honors.'
			}
		},
		websitesSocialLinks: {
			title: 'Websites and social links',
			sectionDescription:
				'You can add links to websites that recruiters will see! This can be a link to your portfolio, your LinkedIn profile or your personal website.',
			addSlot: 'Add a link',
			name: {
				label: 'Site or social media'
			},
			link: {
				label: 'Link'
			}
		},
		skills: {
			title: 'Skills',
			sectionDescription:
				'List your skills and experience levels to visualize your strengths and optimize your keywords.',
			addSlot: 'Add a skill',
			skill: {
				label: 'Skill'
			},
			level: {
				label: 'Niveau',
				'1': 'Beginner',
				'2': 'Average',
				'3': 'Experienced',
				'4': 'Expert'
			}
		},
		coursesHistory: {
			title: 'Certificates',
			addSlot: 'Add a certificate',
			postTitle: {
				label: 'Certificate'
			},
			host: {
				label: 'School'
			},
			duration: {
				label: 'Start and End Date',
				hint: 'If you do not want to display the month, you can type the year only. Leave the field blank if you do not want to display the dates for this entry. <br /> Example : « March 2016 », « 2016 » or empty'
			}
		},
		trainingsHistory: {
			title: 'Internships',
			addSlot: 'Add an internship',
			postTitle: {
				label: 'Job title'
			},
			host: {
				label: 'Employer'
			},
			duration: {
				label: 'Start and End Date',
				hint: 'If you do not want to display the month, you can type the year only. Leave the field blank if you do not want to display the dates for this entry. <br /> Example : « March 2016 », « 2016 » or empty'
			},
			description: {
				label: 'Description',
				placeholder:
					'e.g., creating and implementing lesson plans based on children interests and curiosities.',
				tip: 'Recruiters tip: write a 200+ character text that includes mainly the most relevant keywords for the offered position.'
			}
		},
		languages: {
			title: 'Languages',
			addSlot: 'Add a language',
			lang: {
				label: 'Language'
			},
			level: {
				label: 'Level',
				'1': 'Beginner',
				'2': 'Average',
				'3': 'Very Skilled',
				'4': 'Native Speaker'
			}
		},
		fun: {
			title: 'Leisure',
			sectionDescription: 'What hobby do you like to do?',
			placeholder: 'e.g. Skiing, Fishing, Painting'
		},
		extraWorkHistory: {
			title: 'Extra-professional activities',
			addSlot: 'Add an activity',
			postTitle: {
				label: 'Job Title'
			},
			host: {
				label: 'Employer'
			},
			duration: {
				label: 'Start and End Date',
				hint: 'If you do not want to display the month, you can type the year only. Leave the field blank if you do not want to display the dates for this entry. <br /> Example : « March 2016 », « 2016 » or empty'
			},
			city: {
				label: 'City'
			},
			description: {
				label: 'Description'
			}
		},
		customSection: {
			title: 'Untitled',
			addSlot: 'Add an element',
			postTitle: {
				label: 'Activity name, job title, book title, etc.'
			},
			city: {
				label: 'City'
			},
			duration: {
				label: 'Start and End Date',
				hint: 'If you do not want to display the month, you can type the year only. Leave the field blank if you do not want to display the dates for this entry. <br /> Example : « March 2016 », « 2016 » or empty'
			},
			description: {
				label: 'Description'
			}
		}
	}
};
