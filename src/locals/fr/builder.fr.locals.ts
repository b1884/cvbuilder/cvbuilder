export default {
	untitled: 'Sans titre',
	misc: {
		toPresent: 'Présent',
		worksHere: 'Travaille actuellement ici'
	},
	addSection: 'Ajouter une section',
	extraSections: {
		custom: 'Section Personnalisée',
		courses: 'Formations',
		extraActivities: 'Activités extra-Professionnelles',
		trainings: 'Stages',
		fun: 'Loisirs',
		languages: 'Langues'
	},
	additionalInfo: {
		show: 'Afficher les informations supplémentaires',
		hide: 'Masquer les informations supplémentaires'
	},
	sections: {
		personalInfo: {
			title: 'Détails personnels',
			postTitle: {
				label: 'Titre du profil',
				placeholder: 'par ex: Enseignant',
				hint:
					'Ajoutez un titre comme « Développeur Fullstack » ou « Directeur de marketing » qui décrit rapidement ' +
					'votre expérience globale ou le type de poste auquel vous postulez'
			},
			addProfileImage: {
				add: 'Télécharger une photo',
				edit: 'Modifier la photo',
				remove: 'Supprimer la photo'
			},
			lastName: {
				label: 'Nom de famille'
			},
			firstName: {
				label: 'Prénom'
			},
			email: {
				label: 'E-mail'
			},
			phone: {
				label: 'Téléphone'
			},
			country: {
				label: 'Pays'
			},
			city: {
				label: 'Ville'
			},
			address: {
				label: 'Adresse'
			},
			zipCode: {
				label: 'Code Postal'
			},
			drivingLicense: {
				label: 'Permis de Conduire',
				hint: "Remplir cette section si le poste exige un certain type de permis. Si ce n'est pas le cas, laissez-la vide."
			},
			nationality: {
				label: 'Nationalité',
				hint: "N'indiquez votre nationalité que si elle est pertinente pour le poste. Dans la plupart des cas, laissez ce champ vide."
			},
			birthLocation: {
				label: 'Lieu De Naissance'
			},
			birthDate: {
				label: 'Date De Naissance',
				hint: "N'ajoutez votre date de naissance que si c'est une exigence pertinente pour le poste. Dans la plupart des cas, laissez ce champ vide."
			}
		},
		personalResume: {
			title: 'Résumé',
			sectionDescription: 'Inclure 2 ou 3 phrases claires au sujet de votre expérience globale',
			placeholder:
				"par ex. Enseignant ayant 5 ans d'expérience et maitrisant les pédagogies constructivistes, ..."
		},
		employmentHistory: {
			title: 'Expérience professionnelle',
			sectionDescription:
				'Expliquez vos dernières expériences professionnelles qui sont les plus pertinentes pour le poste.' +
				" Mentionnez les dates et commencez par l'expérience la plus récente",
			addSlot: 'Ajouter un emploi',
			postTitle: {
				label: 'Titre du Poste'
			},
			host: {
				label: 'Employeur'
			},
			duration: {
				label: 'Date de Début et de Fin',
				hint:
					"Si vous ne souhaitez pas afficher le mois, vous pouvez taper l'année uniquement." +
					'Laissez le champ vide si vous ne voulez pas afficher les dates pour cette saisie. <br /> Exemple : « février 2016 », « 2016 » ou vide'
			},
			city: {
				label: 'Ville'
			},
			description: {
				label: 'Description',
				placeholder:
					'par. ex. Création et mise en œuvre de plans de leçon fondés sur les intérêts et les curiosités des enfants.',
				tip: "Conseil: Incluez plus de 200 caractères pour augmenter les chances d'entretien"
			}
		},
		educationHistory: {
			title: 'Formation',
			sectionDescription:
				"S'il y a lieu, indiquez vos plus récents diplômes et expliquez l'objet de la formation.",
			addSlot: 'Ajouter une formation',
			postTitle: {
				label: 'Diplôme'
			},
			host: {
				label: 'École'
			},
			duration: {
				label: 'Date de Début et de Fin',
				hint: "Si vous ne souhaitez pas afficher le mois, vous pouvez taper l'année uniquement. Laissez le champ vide si vous ne voulez pas afficher les dates pour cette saisie. <br /> Exemple : « février 2016 », « 2016 » ou vide"
			},
			city: {
				label: 'Ville'
			},
			description: {
				label: 'Description',
				placeholder: 'Par exemple, diplômé avec mention très bien.'
			}
		},
		websitesSocialLinks: {
			title: 'Sites web et liens sociaux',
			sectionDescription:
				'Vous pouvez ajouter des liens vers des sites web que les recruteurs examineront! ' +
				'Cela peut être un lien vers votre portfolio, votre profil LinkedIn ou votre site web personnel.',
			addSlot: 'Ajouter un lien',
			name: {
				label: 'Nom du site ou réseau'
			},
			link: {
				label: 'Lien'
			}
		},
		skills: {
			title: 'Compétences',
			sectionDescription:
				"Dressez la liste de vos compétences et de vos niveaux d'expérience afin de visualiser vos forces. Et choisissez les compétences les plus pertinentes pour le poste",
			addSlot: 'Ajouter une compétence',
			skill: {
				label: 'Compétence'
			},
			level: {
				label: 'Niveau',
				'1': 'Débutant(e)',
				'2': 'Compétent(e)',
				'3': 'Expérimenté',
				'4': 'Expert'
			}
		},
		coursesHistory: {
			title: 'Certifications',
			addSlot: 'Ajouter une certification',
			postTitle: {
				label: 'Certification'
			},
			host: {
				label: 'Établissement'
			},
			duration: {
				label: 'Date de Début et de Fin',
				hint: "Si vous ne souhaitez pas afficher le mois, vous pouvez taper l'année uniquement. Laissez le champ vide si vous ne voulez pas afficher les dates pour cette saisie. <br /> Exemple : « février 2016 », « 2016 » ou vide"
			}
		},
		trainingsHistory: {
			title: 'Stages',
			addSlot: 'Ajouter un stage',
			postTitle: {
				label: 'Titre du Poste'
			},
			host: {
				label: 'Employeur'
			},
			duration: {
				label: 'Date de Début et de Fin',
				hint: "Si vous ne souhaitez pas afficher le mois, vous pouvez taper l'année uniquement. Laissez le champ vide si vous ne voulez pas afficher les dates pour cette saisie. <br /> Exemple : « février 2016 », « 2016 » ou vide"
			},
			description: {
				label: 'Description',
				placeholder:
					'p. ex. Création et mise en œuvre de plans de leçon fondés sur les intérêts et les curiosités des enfants.',
				tip: "Conseil: Incluez plus de 200 caractères pour augmenter les chances d'entretien"
			}
		},
		languages: {
			title: 'Langues',
			addSlot: 'Ajouter une langue',
			lang: {
				label: 'Langue'
			},
			level: {
				label: 'Niveau',
				'1': 'Débutant(e)',
				'2': 'Intermédiaire',
				'3': 'Très Bonne Maîtrise',
				'4': 'Langue maternelle'
			}
		},
		fun: {
			title: "Centres d'intérêt",
			sectionDescription: 'Quelle activité aimez-vous pratiquer ?',
			placeholder: 'par ex. Ski, Saut en parachute, Peinture'
		},
		extraWorkHistory: {
			title: 'Activités extra-Professionnelles',
			addSlot: 'Ajouter une activité',
			postTitle: {
				label: 'Titre du Poste'
			},
			host: {
				label: 'Employeur'
			},
			duration: {
				label: 'Date de Début et de Fin',
				hint: "Si vous ne souhaitez pas afficher le mois, vous pouvez taper l'année uniquement. Laissez le champ vide si vous ne voulez pas afficher les dates pour cette saisie. <br /> Exemple : « février 2016 », « 2016 » ou vide"
			},
			city: {
				label: 'Ville'
			},
			description: {
				label: 'Description'
			}
		},
		customSection: {
			title: 'Sans titre',
			addSlot: 'Ajouter un élément',
			postTitle: {
				label: "Nom de l'activité, titre du poste, titre du livre, etc."
			},
			city: {
				label: 'Ville'
			},
			duration: {
				label: 'Date de Début et de Fin',
				hint: "Si vous ne souhaitez pas afficher le mois, vous pouvez taper l'année uniquement. Laissez le champ vide si vous ne voulez pas afficher les dates pour cette saisie. <br /> Exemple : « février 2016 », « 2016 » ou vide"
			},
			description: {
				label: 'Description'
			}
		}
	}
};
