export interface TemplateFileStatsInterface {
	name: string;
	path: string;
	lastUpdateDate: Date;
	creationDate: Date;
	codeName: string;
}
