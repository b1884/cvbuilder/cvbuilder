import { UserInterface } from '../models';

export interface VerifyEmailPayloadInterface {
	key: string;
}
export interface CheckEmailExistPayloadInterface {
	email: string;
}

export interface VerifyNewEmailPayloadInterface {
	verifyEmail: string;
}

export interface SignUpPayloadInterface extends UserInterface {
	codeName?: string;
}

export interface GetLinkedinTokenPayloadInterface {
	code: string;
	signUp: boolean;
}

export interface SignInPayloadInterface {
	email: string;
}

export interface SignInResponseInterface {
	iat: number;
	exp: number;
}

export interface SignUpResponseInterface extends SignInResponseInterface {
	resumeLink: string;
}
