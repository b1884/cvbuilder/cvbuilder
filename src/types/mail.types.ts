export interface RecipientInterface {
	email: string;
	name: string;
	type: 'to' | 'cc' | 'bcc';
}

export interface MessageInterface {
	html?: string;
	text?: string;
	subject: string;
	from_email: string;
	from_name: string;
	to: RecipientInterface[];
}

export interface MailInterface {
	key: string;
	message: MessageInterface;
}

export interface MailResponseInterface {
	email: string;
	status: 'sent' | 'queued' | 'scheduled' | 'rejected' | 'invalid';
	_id: string;
	reject_reason:
		| 'hard-bounce'
		| 'soft-bounce'
		| 'spam'
		| 'unsub'
		| 'custom'
		| 'invalid-sender'
		| 'invalid'
		| 'test-mode-limit'
		| 'unsigned'
		| 'rule';
}

export interface MailErrorInterface {
	status: 'error';
	message: string;
	code: number;
	name: 'Invalid_Key' | 'PaymentRequired' | 'Unknown_Subaccount' | 'ValidationError' | 'GeneralError';
}
