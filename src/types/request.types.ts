import * as e from 'express';
import { UserInterface } from '../models';

export interface CvBuilderCookiesInterface {
	handler_crow?: string;
	Domain?: string;
	lang?: string;
	iat?: number;
	exp?: number;
}

export interface CvBuilderRequestInterface<T = unknown, R = unknown, P = unknown, RES = unknown>
	extends e.Request<P, RES, T, R> {
	file?: Express.Multer.File;
	user?: Pick<UserInterface, '_id'>;
	cookies: CvBuilderCookiesInterface;
}
