import { LangType } from './lang.types';

export interface SlotInterface {
	name?: string;
	link?: string;
	skill?: string;
	levelInText?: string;
	lang?: string;
	level?: string;
	postTitle?: string;
	host?: string;
	duration?: string;
	durationEnd?: string;
	city?: string;
	description?: string;
	index?: string;
}

export interface FieldValueInterface {
	label?: string;
	value?: string;
	show?: boolean;
}

export interface FieldInterface {
	title: string;
	customTitle?: string;
	code: string;
	desiredPostTitle?: string;
	firstName?: string;
	lastName?: string;
	email?: string;
	phone?: string;
	country?: string;
	city?: string;
	address?: string;
	zipCode?: string;
	drivingLicense?: string;
	nationality?: string;
	birthLocation?: string;
	birthDate?: string;
	personalResume?: string;
	slots?: SlotInterface[];
	[key: string]: FieldValueInterface | string | SlotInterface[] | undefined;
}

export interface ResumeInputInterface {
	title: string;
	stylesheetLink?: string;
	profileImage?: string;
	language: LangType;
	fields: FieldInterface[];
}
