import { TemplateInterface } from '../models';

export type PreviewLinksType = Pick<TemplateInterface, 'previewSm' | 'previewXl'>;
