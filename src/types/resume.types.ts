import { ClassicResumeInterface } from '../models';

export type UniqueLinksType = Pick<ClassicResumeInterface, 'link' | 'sharingLink'>;
