import { RedisClientType } from 'redis';

export type CvBuilderGlobalType = typeof globalThis & { __appRoot: string; redisClient?: RedisClientType };
